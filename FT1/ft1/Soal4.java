package ft1;

import java.util.Scanner;

public class Soal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("n :");
		int n = input.nextInt();
		System.out.println("x :");
		int x = input.nextInt();

		int lembar = 0;

		for (int i = 0; i < n; i++) {
			if (i % 2 == 1 && i <= x) {
				lembar++;
			}
		}
		
		System.out.println("Lembar Ke-" + lembar);

	}

}
