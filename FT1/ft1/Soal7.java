package ft1;

import java.util.Scanner;

public class Soal7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("Kalimat :");
		String kalimat = input.nextLine();

		String newKalimat = kalimat.replaceAll("[,'@/&]", "");
		String arrNewKalimat[] = newKalimat.split(" ");

		String result = "";

		for (int i = 1; i < arrNewKalimat.length; i++) {
			if (!arrNewKalimat[i].equals(arrNewKalimat[i-1])) {
				result += " " + arrNewKalimat[i];
			}
		}

		System.out.println(arrNewKalimat[0] + result);

	}

}
