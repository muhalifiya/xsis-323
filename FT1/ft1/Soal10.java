package ft1;

import java.util.Scanner;

public class Soal10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Kalimat :");
		String Kalimat = input.nextLine().toLowerCase().replaceAll("[? ]", "");
		System.out.println(Kalimat);

		int hitung = 0;

		for (int i = 1; i < Kalimat.length(); i++) {
			if (Kalimat.charAt(i) != 'a' || Kalimat.charAt(i) != 'i' || Kalimat.charAt(i) != 'u'
					|| Kalimat.charAt(i) != 'e' || Kalimat.charAt(i) != 'o') {
				System.out.print(Kalimat.charAt(i));
				if (Kalimat.charAt(i - 1) == 'a' || Kalimat.charAt(i - 1) == 'i' || Kalimat.charAt(i - 1) == 'u'
						|| Kalimat.charAt(i - 1) == 'e' || Kalimat.charAt(i - 1) == 'o') {
					System.out.print(Kalimat.charAt(i-1));
					hitung++;
				}
			}
			System.out.println();
		}
		
		System.out.println(hitung);

	}

}
