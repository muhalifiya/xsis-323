create table type_dosen(
	id serial not null primary key,
	kode_typeDosen varchar(10)UNIQUE,
	deskripsi varchar(20)
);

create table dosen(
id serial not null primary key,
kode_dosen varchar(20) UNIQUE,
nama_dosen varchar(50),
kode_jurusan varchar(10) references jurusan(kode_jurusan),
kode_type_dosen varchar(10) references type_dosen(kode_typeDosen)
);

create table jurusan(
id serial not null primary key,
kode_jurusan varchar(20) UNIQUE,
nama_jurusan varchar(50),
status_jurusan varchar(10)
);

drop table jurusan

create table ujian(
id serial not null primary key,
kode_ujian varchar(20) UNIQUE,
nama_ujian varchar(50),
status_ujian varchar(10)
);

create table nilai(
id serial not null primary key,
kode_mahasiswa varchar(10) references mahasiswa(kode_mahasiswa),
kode_ujian varchar(50),
nilai int
);

create table agama(
id serial not null primary key,
kode_agama varchar(10) UNIQUE,
deskripsi varchar(15)
);

create table mahasiswa(
	id serial not null primary key,
	kode_mahasiswa varchar(10) UNIQUE,
	nama_mahasiswa varchar(50),
	alamat varchar(100),
	kode_agama varchar(10) references agama(kode_agama),
	kode_jurusan varchar(20) references jurusan(kode_jurusan)
);

INSERT INTO dosen (kode_dosen, nama_dosen, kode_jurusan, kode_type_dosen) VALUES
('D001', 'Prof. Dr. Retno Wahyuningsih', 'J001', 'T002'),
('D002', 'Prof. Roy M. Sutikno', 'J002', 'T001'),
('D003', 'Prof. Hendri A. Verburgh', 'J003', 'T002'),
('D004', 'Prof. Risma Suparwata', 'J004', 'T002'),
('D005', 'Prof. Amir Sjarifuddin Madjid, MM, MA', 'J005', 'T001');

INSERT INTO jurusan (kode_jurusan, nama_jurusan, status_jurusan) VALUES
('J001', 'Teknik Informatika', 'Aktif'),
('J002', 'Management Informatika', 'Aktif'),
('J003', 'Sistem Informatika', 'Non Aktif'),
('J004', 'Sistem Komputer', 'Aktif'),
('J005', 'Komputer Akuntansi', 'Non Aktif');

INSERT INTO mahasiswa (kode_mahasiswa, nama_mahasiswa, alamat, kode_agama, kode_jurusan) VALUES
('M001', 'Budi Gunawan', 'jl. Mawar No 3 RT 05 Cicalengka, Bandung', 'A001', 'J001'),
('M002', 'Rinto Raharjo', 'jl. Kebagusan No. 33 RT04 RW06 Bandung', 'A002', 'J002'),
('M003', 'Asep Saepudin', 'jl. Sumatera No. 12 RT02 RW01 Ciamis', 'A001', 'J003'),
('M004', 'M. Hafif Isbullah', 'jl. Jawa No 1 RT01 RW01, Jakata Pusat', 'A001', 'J001'),
('M005', 'Cahyono', 'jl. Niagara No. 54 RT01 RW09, Surabaya', 'A003', 'J002');

INSERT INTO type_dosen (kode_typeDosen, deskripsi) VALUES
('T001', 'Tetap'),
('T002', 'Honorer'),
('T003', 'Expertise');

INSERT INTO ujian (kode_ujian, nama_ujian, status_ujian) VALUES
('U001', 'Algoritma', 'Aktif'),
('U002', 'Aljabar', 'Aktif'),
('U003', 'Statistika', 'Non Aktif'),
('U004', 'Etika Profesi', 'Non Aktif'),
('U005', 'Bahasa Inggris', 'Aktif');

INSERT INTO nilai (kode_mahasiswa, kode_ujian, nilai) VALUES
('M004', 'U001', 90),
('M001', 'U001', 80),
('M002', 'U003', 85),
('M004', 'U002', 95),
('M005', 'U005', 70);

INSERT INTO agama (kode_agama, deskripsi) VALUES
('A001', 'Islam'),
('A002', 'Kristen'),
('A003', 'Katolik'),
('A004', 'Hindu'),
('A005', 'Budha');

select * from jurusan
----------------------------------------------------------------------------------------------
-----DQL (Data Query Language)

select * from mahasiswa;
select kode_mahasiswa, nama_mahasiswa from mahasiswa;
select kode_mahasiswa, kode_jurusan from mahasiswa where kode_jurusan = 'J001';
select kode_mahasiswa, kode_jurusan from mahasiswa where kode_jurusan = 'J001';
select kode_mahasiswa, kode_jurusan from mahasiswa where kode_jurusan = 'J001' or kode_jurusan = 'J003';
select kode_mahasiswa, kode_jurusan, kode_agama from mahasiswa where kode_agama = 'A003' and kode_jurusan = 'J002';

select * from mahasiswa where id > 2;
select * from mahasiswa where id > 1 and id < 3;
select * from mahasiswa where id >= 1 and id <= 3;
select * from mahasiswa where id >= 1 and id <= 2 or id = 4;
select * from mahasiswa where id = (2+2); -- hanya contoh tidak baik dilakukan
select * from mahasiswa where kode_mahasiswa = 'M001' and kode_agama = 'A001' and kode_jurusan = 'J001';
select * from mahasiswa where kode_mahasiswa <> 'M001'; -- tampilkan mahasiswa selain M001
select * from mahasiswa where id in (1,3,5);
select * from mahasiswa where id not in (1,3,5); -- negasi dari in
select * from mahasiswa where id between 1 and 3;
select * from mahasiswa where id not between 1 and 3;
select 1 as _number;
select 'A' as _char;
select 'text' as _String;
select md5('password123') as hashpassword;
select sha256('blue'::bytea) as shapass;
select 'hello world';
select 1,2,3; -- koma menandakan pemisahan kolom
select * from mahasiswa where id in (select id from mahasiswa where id >= 1 and id <= 3); -- select sebagai parameter expression
select * from mahasiswa where id <= 3 order by id;
select * from mahasiswa order by nama_mahasiswa ; -- order by membuat urutan keluaran menjadi asc atau desc
select * from mahasiswa order by nama_mahasiswa desc ;
select 'bogor' as kota, * from mahasiswa; -- hardcode 'bogor' sebagai kolom kota
select (id*5) as id5, * from mahasiswa;
select id, nama_mahasiswa, (id/2) as id5, kode_jurusan from mahasiswa;
------String
select kode_mahasiswa from mahasiswa where nama_mahasiswa = 'Cahyono';
select kode_mahasiswa from mahasiswa where nama_mahasiswa like 'Cahyono'; -- like -> wildcard
select nama_mahasiswa, alamat from mahasiswa where alamat  like '%Bandung'; -- % di depan menandakan cari apa saja yang teks akhirnya berkaitan dengan Bandung
select nama_mahasiswa, alamat from mahasiswa where alamat  like 'jl. Mawar%'; -- % di akhir kebalikan % di depan
select nama_mahasiswa, alamat from mahasiswa where alamat  like '%No%'; -- diantara
select nama_mahasiswa, alamat from mahasiswa where alamat  like '%Jakata%';
select nama_mahasiswa, alamat from mahasiswa where alamat  ilike '%jakata%'; -- ilike membiarkan/membuang case sensitif
------Aggregasi (aggregate) / menghitung data yang terkelompok
select * from nilai
select sum(nilai) as total_nilai from nilai; -- penjumlahan
select sum(nilai) as total_nilai_m004 from nilai where kode_mahasiswa = 'M004';
select avg(nilai) as avg_nilai from nilai; -- rata-rata
select avg(nilai) as avg_nilai_m004 from nilai where kode_mahasiswa = 'M004';
select max(nilai) as max_nilai_ from nilai; -- maksimum nilai
select max(nilai) as max_nilai_m004 from nilai where kode_mahasiswa = 'M004';
select max(nilai) as max_nilai from nilai where nilai <> (select max(nilai)from nilai); -- <> tidak sama dengan
select min(nilai) as min_nilai from nilai; -- minimal nilai
select min(nilai) as min_nilai from nilai where kode_mahasiswa = 'M004';
select count(nilai) as banyaknya_nilai from nilai; -- menghitung nilai yang ada
select count(nilai) as banyaknya_nilai from nilai where kode_mahasiswa = 'M004';
------Grouping / Pengelompokkan
select nama_mahasiswa, kode_jurusan from mahasiswa;
select kode_jurusan, count(kode_jurusan) as jumlah_mahasiswa 
from mahasiswa group by kode_jurusan;
select kode_mahasiswa, sum(nilai) from nilai group by kode_mahasiswa;
select kode_mahasiswa, avg(nilai) from nilai group by kode_mahasiswa;
select kode_mahasiswa, min(nilai) from nilai group by kode_mahasiswa;
select kode_mahasiswa, max(nilai) from nilai group by kode_mahasiswa;
select kode_mahasiswa, avg(nilai) from nilai where nilai > 90 group by kode_mahasiswa;
select kode_mahasiswa, avg(nilai) from nilai where nilai <= 80 group by kode_mahasiswa;
select kode_mahasiswa, avg(nilai) from nilai group by kode_mahasiswa having avg(nilai) > 90; -- having mendapatkan kondisi dari suatu aggregat
------Join (menggabungkan table yang berelasi, dengan kondisi tertentu(foreign key dan primary key nya bertemu atau tidak))
-- tanpa foreign key bisa dilakukan join asalnya penyambungnya tidak salah / harus sama
-- foreign key untuk menjaga agar inputan user tidak salah
select 
m.nama_mahasiswa,
j.nama_jurusan,
a.deskripsi 
from mahasiswa m, jurusan j, agama a 
where m.kode_jurusan = j.kode_jurusan and m.kode_agama = a.kode_agama;
--keyword join
select 
m.nama_mahasiswa,
j.nama_jurusan,
a.deskripsi 
from mahasiswa m
join jurusan j on j.kode_jurusan = m.kode_jurusan
join agama a on a.kode_agama = m.kode_agama;

-- explain untuk melihat performa
--explain
select 
m.nama_mahasiswa,
j.nama_jurusan,
a.deskripsi 
from mahasiswa m
inner join jurusan j on j.kode_jurusan = m.kode_jurusan
inner join agama a on a.kode_agama = m.kode_agama;

select 
m.nama_mahasiswa,
j.nama_jurusan,
a.deskripsi 
from mahasiswa m
left join jurusan j on j.kode_jurusan = m.kode_jurusan -- acuan nya berdasarkan table di sebelah kiri(kanan)
left join agama a on a.kode_agama = m.kode_agama;

select 
m.nama_mahasiswa,
j.nama_jurusan,
a.deskripsi 
from mahasiswa m
right join jurusan j on j.kode_jurusan = m.kode_jurusan
right join agama a on a.kode_agama = m.kode_agama;

select 
m.nama_mahasiswa,
j.nama_jurusan
from mahasiswa m
right join jurusan j on j.kode_jurusan = m.kode_jurusan; -- acuan nya berdasarkan table di sebelah kanan(right)
------Sub query (seperti join dengan membuka table dalam kolom)
select
m.nama_mahasiswa,
(select nama_jurusan from jurusan where kode_jurusan = m.kode_jurusan) as nama_jurusan,
(select deskripsi from agama where kode_agama = m.kode_agama) as agama
from mahasiswa m


------union (jumlah table harus sama)
select kode_mahasiswa, kode_ujian, nilai from nilai;
select sum(nilai) from nilai;

select kode_mahasiswa, kode_ujian, nilai from nilai
union all
select ' ' as kode_mahasiswa, 'TOTAL' as kode_ujian, sum(nilai) as nilai from nilai;

select kode_mahasiswa, kode_ujian, nilai from nilai
union all
select '' as kode_mahasiswa, 'TOTAL' as kode_ujian, sum(nilai) as nilai from nilai
union all
select '' as kode_mahasiswa, 'AVERAGE' as kode_ujian, avg(nilai) as nilai from nilai;

------Case when
select
	kode_mahasiswa,
	nilai,
	case when nilai >= 85 then 'Sangat Baik'
		 when nilai >= 71 and nilai <= 84 then 'Kurang'
		 when nilai >= 50 and nilai <= 70 then 'Gagal'
	end keterangan
from nilai;

------View (seperti virtual table, independent ke table aslinya)
create or replace view view_nilai as
select
	kode_mahasiswa,
	nilai,
	case when nilai >= 85 then 'Sangat Baik'
		 when nilai >= 71 and nilai <= 84 then 'Kurang'
		 when nilai >= 50 and nilai <= 70 then 'Gagal'
	end keterangan
from nilai;

select * from view_nilai vn;
select * from view_nilai vn where keterangan = 'Sangat Baik';

create or replace view view_nama as
select
	nama_mahasiswa,
	nilai,
	case when nilai >= 85 then 'Sangat Baik'
		 when nilai >= 71 and nilai <= 84 then 'Kurang'
		 when nilai >= 50 and nilai <= 70 then 'Gagal'
	end keterangan
from nilai n 
join mahasiswa m on m.kode_mahasiswa = n.kode_mahasiswa ;

select * from view_nama;
select * from view_nama where keterangan = 'Sangat Baik';


------------------------------------------------------------------------------------
--1

--2
select * from dosen

alter table dosen alter column nama_dosen type varchar(200);

--3
select * from mahasiswa
select * from jurusan 
select * from agama

select kode_mahasiswa, nama_mahasiswa, nama_jurusan, deskripsi as agama 
from mahasiswa m
join jurusan j on j.kode_jurusan = m.kode_jurusan 
join agama a on a.kode_agama = m.kode_agama
where nama_mahasiswa ilike 'budi gunawan'


--4
select * from mahasiswa
select * from jurusan 

select nama_mahasiswa, nama_jurusan, status_jurusan
from mahasiswa m
join jurusan j on j.kode_jurusan = m.kode_jurusan 
where status_jurusan ilike 'non aktif'

--5
select * from mahasiswa
select * from ujian
select * from nilai

select nama_mahasiswa, nilai, status_ujian
from mahasiswa m
join nilai n on n.kode_mahasiswa = m.kode_mahasiswa 
join ujian u on u.kode_ujian = n.kode_ujian 
where nilai > 80 and status_ujian ilike 'aktif'

--6
select * from jurusan 

select * 
from jurusan 
where nama_jurusan ilike'%sistem%'

--7
select * from mahasiswa
select * from nilai 
select * from ujian

select nama_mahasiswa, count(kode_ujian)
from mahasiswa m
join nilai n on n.kode_mahasiswa = m.kode_mahasiswa
group by nama_mahasiswa 
having count(kode_ujian) > 1

--8
select * from mahasiswa
select * from jurusan
select * from agama
select * from dosen
select * from type_dosen 

select kode_mahasiswa, nama_mahasiswa, nama_jurusan, a.deskripsi, nama_dosen, status_jurusan, td.deskripsi
from mahasiswa m
join jurusan j on j.kode_jurusan = m.kode_jurusan 
join agama a on a.kode_agama = m.kode_agama 
join dosen d on d.kode_jurusan = j.kode_jurusan 
join type_dosen td on td.kode_typedosen = d.kode_type_dosen 
where kode_mahasiswa ilike'm001'

--9
create view query_soal_no8 as
select 
kode_mahasiswa, 
nama_mahasiswa, 
nama_jurusan, 
a.deskripsi as agama, -- karena nama kolom tidak boleh sama maka berikan alias (as)
nama_dosen, 
status_jurusan,
td.deskripsi 
from mahasiswa m
join jurusan j on j.kode_jurusan = m.kode_jurusan
join agama a on a.kode_agama = m.kode_agama
join dosen d on d.kode_jurusan = j.kode_jurusan
join type_dosen td on td.kode_typedosen = d.kode_type_dosen


select * from query_soal_no8
where kode_mahasiswa ilike 'm001'

drop view query_soal_no8

	
--10
select * from mahasiswa;
select * from nilai;

select nama_mahasiswa, coalesce(nilai, 0) as nilai
from mahasiswa m 
left join nilai n on n.kode_mahasiswa = m.kode_mahasiswa
order by nilai desc
------------------------------------------------------------

create schema transaction_;

create table transaction_.accounts(
id serial primary key not null,
name varchar(30) not null,
balance dec(8,2) not null
);

insert into transaction_.accounts (name,balance)
values('Peter',10000);

begin; --begin transaction_ (memulai)
--begin transaction;
--begin work;

--transaction
insert into transaction_.accounts (name,balance)
values('Alice',10000);

--commit work;
--commit transaction;
commit; --commit (diserahkan/dijalankan/memasukkan ke server postgreSql)


select * from transaction_.accounts a;
--------------------------------------------------------------------
insert into transaction_.accounts (name,balance)
values('Jack',0);

begin;
update transaction_.accounts set balance = balance - 1500 where id = 1;
update transaction_.accounts set balance = balance + 1500 where id = 4;
rollback;
commit;