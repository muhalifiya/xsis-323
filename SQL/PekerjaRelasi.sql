create database pekerjaRelasi;

create table position(
id serial primary key not null,
name varchar(20)
);

create table biodata(
id serial primary key not null,
first_name varchar(20),
last_name varchar(20),
dob date,
pob varchar(30),
address varchar(100),
marital_status boolean
);

create table employee(
id serial primary key not null,
biodata_id bigint references biodata(id),
nip varchar(10),
status varchar(10),
slaray numeric(10)
);

create table employee_position(
id serial primary key not null,
employee_id bigint references employee(id),
position_id bigint references position(id)
);

create table family(
id serial primary key not null,
biodata_id bigint references biodata(id),
name varchar(50),
status varchar(50)
);



create table contact_person(
id serial primary key not null,
biodata_id bigint references biodata(id),
type varchar(5),
contact varchar(100)
);

create table department( 
id serial primary key not null,
name varchar(20)
);

create table employee_leave( 
id serial primary key not null,
employee_id bigint references employee(id),
period varchar(10),
regular_quota int
);

create table travel_type(
id serial primary key not null,
name varchar(30),
travel_fee int
);

create table travel_request( 
id serial primary key not null,
employee_id bigint references employee(id),
travel_type_id bigint references travel_type(id),
start_date date,
end_date date
);

create table travel_settlement(
id serial primary key not null,
travel_request_id bigint references travel_request(id),
item_name varchar(50),
item_cost int
);


create table leave( 
id serial primary key not null,
type varchar(20),
name varchar(50)
);


create table leave_request( 
id serial primary key not null,
employee_id bigint references employee(id),
leave_id bigint references leave(id),
start_date date,
end_date date,
reason varchar(50)
);


-----------------------------------------
insert into position(name)
values ('Direktur'),
('General Manager'),
('Manager'),
('Staff')

insert into biodata(first_name,last_name,dob,pob,address,marital_status) values
('Raya','Indriyani','1991/01/01','Bali','Jl. Raya Baru, Bali',false),
('Rere','Wulandari','1992/01/02','Bandung','Jl. Berkah Rhamadan, Bandung',false),
('Bunga','Maria','1991/03/03','Jakarta','Jl. Mawar Semerbak, Bogor',true),
('Natasha','Wulan','1990/04/10','Jakarta','Jl. Mawar Harum, Jakarta',false),
('Zahra','Aurelia Putri','1991/03/03','Jakarta','Jl. Mawar Semerbak, Bogor',true),
('Katniss','Everdeen','1989/01/12','Jakarta','Jl. Bunga Melati, Jakarta',true);

insert into employee(biodata_id,nip,status,slaray) values
(1,'NX001','Permanen', 12000000),
(2,'NX002','Kontrak', 15000000),
(4,'NX003','Permanen', 13500000),
(5,'NX004','Permanen', 12000000),
(6,'NX005','Permanen', 17000000)


insert into employee_position(employee_id,position_id) values
(5,1),
(4,2),
(3,3),
(2,4),
(1,4)

insert into family(biodata_id,name,status) values
(3,'Azka Fadlan Zikrulllah','Suami'),
(3,'Primrose Everdeen','Anak'),
(5,'Jaka Samudera Buana','Suami'),
(5,'Friska Davira','Anak'),
(5,'Harum Indah Az Zahra','Anak'),
(6,'Adya Pratama Yuda','Suami')


insert into contact_person(biodata_id,type,contact) values
(1,'Mail','raya.indriyani@gmail.com'),
(1,'Phone','085612345678'),
(2,'Mail','rere.wulandari@gmail.com'),
(2,'Phone','081312345678'),
(2,'Phone','087812345678'),
(3,'Mail','bunga.maria@gmail.com'),
(4,'Phone','natasha.wulan@gmail.com'),
(5,'Mail','zahra.putri@gmail.com'),
(6,'Mail','katniss.everdeen@gmail.com')

insert into department(name) values
('Recruitment'),
('Sales'),
('Human Resource'),
('General Affair')


insert into employee_leave(employee_id,period,regular_quota) values
(1,'2020',16),
(2,'2020',12),
(1,'2021',16),
(2,'2021',12),
(4,'2021',12),
(5,'2021',12),
(5,'2021',12)

insert into travel_type(name,travel_fee)values
('In Indonesia',200000),
('Out Indonesia',350000)

insert into travel_request(employee_id,travel_type_id,start_date,end_date)values 
(1,1,'2020/07/07','2020/07/08'),
(1,1,'2020/08/07','2020/08/08'),
(2,2,'2020/04/04','2020/04/07')

insert into travel_settlement(travel_request_id, item_name, item_cost) values 
(1,'Tiket Travel Do-Car berangkat',165000),
(1,'Hotel',750000),
(1,'Tiket Travel Do-Car pulang',165000),
(2,'Tiket Pesawat berangkat',750000),
(2,'Hotel',65000),
(2,'Tiket Pesawat pulang',1250000),
(3,'Tiket Pesawat berangkat',4750000),
(3,'Hotel',6000000),
(3,'Tiket Pesawat pulang',4250000)

insert into leave(type,name) values
('Regular','Cuti Tahunan'),
('Khusus','Cuti Menikah'),
('Khusus','Cuti Haji & Umroh'),
('Khusus','Melahirkan')

insert into leave_request (employee_id,leave_id,start_date,end_date,reason) values
(1, 1, '2020/10/10','2020/10/12','Liburan'),
(1, 1, '2020/11/12','2020/11/15','Acara Keluarga'),
(2, 2, '2020/05/05','2020/05/07','Menikah'),
(2, 1, '2020/09/09','2020/09/13','Touring'),
(2, 1, '2020/12/24','2020/12/25','Acara Keluarga')
---------------------------------------------------------------------------------------

alter table employee rename column slaray to salary;

--1
select b.id, concat(b.first_name, ' ', b.last_name) as fullname, dob, pob, address, marital_status,  e.salary
from employee e
join biodata b on e.biodata_id = b.id

--2
select * from biodata
insert into biodata(first_name, last_name,dob,pob,address,marital_status) values
('Mohammad','Rifki N','1999/11/30','Cirebon','Jl. Raya Lama, Cirebon',false),
('Muhammad','Alifiya','1998/10/22','Lampung','Jl. Sumatera, Lampung',false),
('Naruto','Uzumaki','1987/02/02','Konoha','Jl. Mana, Konoha',true)

insert into employee(biodata_id,nip,status,salary) values
(7,'NX006','Kontrak', 13500000),
(8,'NX007','Kontrak', 12000000)

-- Lalu tampilkan semua biodata berupa fullname, nip, status karyawan dan salary
select concat(b.first_name, ' ', b.last_name) as nama, e.nip, 
e.status as status_karyawan, e.salary as gaji_karyawan
from employee e
join biodata b on b.id = e.biodata_id

--3
select * from biodata
order by dob

select concat(first_name, ' ', last_name) as fullname, dob
from biodata
where dob between '1991/01/01' and '1991/12/31'

--4
select concat(first_name, ' ', last_name) as fullname
from employee e
right join biodata b on e.biodata_id = b.id
where e.biodata_id is null


--5
INSERT INTO leave_request (id,employee_id,leave_id,start_date,end_date,reason)
VALUES (6, 3, 1, '2020/03/01','2020/03/02','Reunian');

select * from leave_request

--6
select * from travel_settlement 

select concat(b.first_name, ' ', b.last_name) as full_name, 
tt.name as jenis_perjalanan_dinas, tr.start_date as tanggal_perjalanan, 
sum(ts.item_cost) as total_Pengeluaran
from biodata b
join employee e on e.biodata_id = b.id
join travel_request tr on tr.employee_id = e.id
join travel_type tt on tt.id = tr.travel_type_id
join travel_settlement ts on ts.travel_request_id = tr.id
group by full_name, jenis_perjalanan_dinas, tanggal_perjalanan

--7
select * from leave_request 

select concat(b.first_name, ' ', b.last_name) as nama, 
el.period as Tahun_terpakai, 
el.regular_quota as Kuota_Cuti_2020, 
sum(lr.end_date - lr.start_date + 1) as Cuti_Terpakai,
el.regular_quota - sum(lr.end_date - lr.start_date + 1) as Sisa_Kuota_Cuti_2020
from employee e
join biodata b on b.id = e.biodata_id
join employee_leave el on el.employee_id = e.id
join leave_request lr on lr.employee_id = e.id
where period = '2020' and lr.leave_id = 1
group by nama, el.period, el.regular_quota


--8
select concat(first_name, ' ', last_name) as fullname, e.status, dob
from biodata b
join employee e on e.biodata_id = b.id
order by dob


--9
select concat(b.first_name, ' ', b.last_name) as full_name, 
sum(ts.item_cost) as total_item_cost,
tt.travel_fee as biaya_perjalanan,
(sum(ts.item_cost) - tt.travel_fee) as selisih,
tt.name as tipe_perjalanan,
tr.start_date as tanggal_mulai_perjalanan,
tr.end_date as tanggal_akhir_perjalanan,
sum(tr.end_date - tr.start_date)
from employee e
join biodata b on b.id = e.biodata_id
join travel_request tr on tr.employee_id = e.id
join travel_type tt on tt.id = tr.travel_type_id
join travel_settlement ts on ts.travel_request_id = tr.id
group by full_name, tipe_perjalanan, biaya_perjalanan, tanggal_mulai_perjalanan, tanggal_akhir_perjalanan

select sum(tr.end_date - tr.start_date) 
from employee e
join travel_request tr on tr.emlpoyee_id = e.id

select e.id, sum(end_date - start_date + 1)
from employee e
join travel_request tr on tr.employee_id = e.id
join biodata.
group by e.id

select concat(first_name, ' ', last_name) as fullname, ty.travel_fee*((tr.end_date - tr.start_date) + 1) as "ongkos harian", 
sum(ts.item_cost) as "ongkos perjalanan", 
sum(ts.item_cost) - ty.travel_fee*((tr.end_date - tr.start_date) + 1) as selisih
from travel_request tr
join travel_settlement ts
on ts.travel_request_id = tr.id
join travel_type ty
on ty.id = tr.travel_type_id
join employee e
on tr.employee_id = e.id
join biodata b
on b.id = e.biodata_id
group by fullname, ty.travel_fee, tr.id;


--10
select * from employee
select * from employee_leave
select * from leave_request

insert into employee_leave(employee_id,period,regular_quota) values
(6,'2021',12),
(7,'2021',12)


select concat(b.first_name,' ',b.last_name) as nama,
count(lr.employee_id) as jumlah_cuti_yang_diambil
from employee e
join leave_request lr on e.id = lr.employee_id
join biodata b on b.id = e.biodata_id
group by nama;
----------------------------------------------------------------------------------------------

--1 tampilkan nama, jabatan, usia dan jumlah anak dari masing" karyawan saat ini
----kalau tidak ada anak tulis 0(nol) atau '-' saja
select * from employee 

select concat(b.first_name, ' ', b.last_name) as nama,
p.name as jabatan, 
((DATE'2020-12-31'-dob)/365) as "Umur",
coalesce(count(f.status)- 1,0) as "Jumlah Anak"
from biodata b
left join family f on f.biodata_id = b.id
join employee e on e.biodata_id = b.id
join employee_position ep on ep.employee_id = e.id
join position p on p.id = ep.position_id
where b.marital_status is true or f.status ilike 'anak'
group by nama, jabatan, dob
order by ((DATE'2020-12-31'-dob)/365) desc 

select concat(b.first_name,' ',b.last_name) as nama_lengkap,
p.name as jabatan,
((DATE'2020-12-31'-dob)/365) as usia,
count(case when f.status ilike '%anak%' then 1 end) as jumlah_anak
from biodata b
join employee e on e.biodata_id = b.id 
join employee_position ep on ep.employee_id = e.id 
join position p on p.id = ep.position_id
left join family f on f.biodata_id = b.id
group by nama_lengkap, p.name, b.dob;


--2 tampilkan nama-nama pelamar yang tidak di terima sebagai karyawan
select concat(b.first_name, ' ', b.last_name) as nama
from biodata b
left join employee e on e.biodata_id = b.id
where e.id is null

--3 hitung berapa rata" gaji karyawan dengan level staff
select  *
from employee e
join employee_position ep on ep.employee_id = e.id
where ep.position_id = 4

select avg(salary) as "Rata-rata Gaji Staff"
from employee e
join employee_position ep on ep.employee_id = e.id
where ep.position_id = 4

--4 hitung berapa rata" gaji karyawan dengan level managerial (non staff)
select  *
from employee e
join employee_position ep on ep.employee_id = e.id
where ep.position_id != 4

select avg(salary) as "Rata-rata Gaji Managerial (Non Staff)"
from employee e
join employee_position ep on ep.employee_id = e.id
where ep.position_id != 4

--5 hitung ada berapa jumlah karyawan yang sudah menidak dan belum menikah
----(tabel : menikah x orang, tidak menikah x orang)
select * from biodata b
join employee e on e.biodata_id = b.id

select (select count(marital_status) 
from biodata b
join employee e on e.biodata_id = b.id 
where marital_status = true) as "Menikah x Orang",
(select count(marital_status) 
from biodata b
join employee e on e.biodata_id = b.id 
where marital_status = false) as "Belum Menikah x Orang"

select 'Menikah', count(marital_status) as "jumlah"
from biodata b
join employee e on e.biodata_id = b.id 
where marital_status = true 
union  all
select 'Belum Menikah', count(marital_status) as "jumlah"
from biodata b
join employee e on e.biodata_id = b.id 
where marital_status = false 

--6 jika digabungkan antara cuti dan perjalanan dinas
----berapa hari raya tidak berada di kantor pada tahun 2020?
select sum(end_date - start_date +1) as total1
from employee e
join leave_request lr on lr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.id = 1

select sum(end_date - start_date +1) as total
from employee e
join travel_request tr on tr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.id = 1

select concat(b.first_name, ' ', b.last_name) as nama,
(select sum(end_date - start_date +1) as total1
from employee e
join leave_request lr on lr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.id = 1) + (select sum(end_date - start_date +1) as total
from employee e
join travel_request tr on tr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.id = 1) as "Tidak berada di Kantor"
from biodata b
where b.first_name ilike '%raya%'

select concat(b.first_name, ' ', b.last_name) as nama,
sum(lr.end_date - lr.start_date +1)+sum(tr.end_date - tr.start_date +1)
from employee e
join leave_request lr on lr.employee_id = e.id
join travel_request tr on tr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.first_name ilike '%raya%'
group by nama

select concat(b.first_name, ' ', b.last_name) as nama,
(sum(tr.end_date - tr.start_date) + sum(lr.end_date - lr.start_date - 1) + 1) as "tidak di kantor"
from employee e
join travel_request tr on e.id = tr.employee_id
join leave_request lr on e.id = lr.employee_id
where lr.employee_id = 1

create view lr_view as 
select concat(b.first_name, ' ', b.last_name) as nama,
sum(end_date - start_date +1) as lr_total
from employee e
join leave_request lr on lr.employee_id = e.id
join biodata b on b.id = e.biodata_id
group by nama

create view tr_view as 
select 
concat(b.first_name, ' ', b.last_name) as nama,
sum(end_date - start_date +1) as tr_total
from employee e
join travel_request tr on tr.employee_id = e.id
join biodata b on b.id = e.biodata_id
group by nama

select * from lr_view 
drop view tr_view 

select nama, lr_total + (select sum(end_date - start_date +1) as tr_total
from employee e
join travel_request tr on tr.employee_id = e.id
join biodata b on b.id = e.biodata_id
where b.first_name ilike '%raya%') as "Tidak berada di Kantor"
from lr_view
where nama ilike '%raya%'


SELECT CONCAT (b.first_name,' ', b.last_name) AS nama_lengkap,
ty.name AS jenis_perjalanan_dinas, tr.start_date AS tgl_perjalanan,
SUM(ts.item_cost) AS total_pengeluaran
FROM biodata b
JOIN employee e
ON e.biodata_id = b.id
JOIN travel_request tr 
ON e.id = tr.employee_id
JOIN travel_type ty
ON tr.travel_type_id = ty.id
JOIN travel_settlement ts
ON ts.travel_request_id = tr.id
GROUP BY nama_lengkap, jenis_perjalanan_dinas, tgl_perjalanan
ORDER BY tr.start_date DESC


select concat(first_name, ' ', last_name) as fullname
from biodata b
left join employee e on e.biodata_id = b.id
where e.biodata_id is null

