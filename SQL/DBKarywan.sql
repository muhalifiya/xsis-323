create database DBKaryawan;
  

create table karyawan(
	id serial not null primary key,
	nomor_induk varchar(10) not null unique,
	nama varchar(30) not null,
	alamat text not null,
	tanggal_lahir date not null,
	tanggal_masuk date not null
);

alter table karyawan rename column nama to nama_karyawan;

drop table cuti_karyawan;
drop table karyawan;


create table cuti_karyawan(
	id serial not null primary key,
	nomor_induk varchar(10) not null references karyawan(nomor_induk),
	tanggal_mulai date not null,
	lama_cuti int not null,
	keterangan text not null
);
   

insert into karyawan (nomor_induk, nama, alamat, tanggal_lahir, tanggal_masuk) values
('IP06001', 'Agus', 'Jln. Gajah Mada 115A, Jakarta Pusat', 8/1/70, 7/7/06), -- bulan/tanggal/tahun
('IP06002', 'Amin', 'Jln. Bungur sari v No, 178, bandung', 5/3/77, 7/6/06),
('IP06003', 'Yusuf', 'Jln. Yosodpuro 15, surabaya', 8/9/73, 7/8/06),
('IP06004', 'Alyssa', 'Jln. Cendana No. 6 Bandung', 2/14/83, 1/5/07),
('IP06005', 'Maulana', 'Jln. Ampera Raya No 1', 10/10/85, 2/5/07),
('IP06006', 'Afika', 'Jln. Pejaten Barat No 6A', 3/9/87, 6/9/07),
('IP06007', 'James', 'Jln. Padjadjaran No. 111, bandung', 5/19/88, 6/9/07),
('IP06008', 'Octavanus', 'Jln. Gajah Mada 101. Semarang', 10/7/88, 8/8/08),
('IP06009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta selatan',1/20/88, 11/11/08),
('IP06010', 'Raisa', 'Jln. Nangka Jakarta selatan', 12/29/89, 2/9/09);

insert into karyawan (nomor_induk, nama, alamat, tanggal_lahir, tanggal_masuk)values 
('IP06001', 'Agus', 'Jln. Gajah Mada 115A, Jakarta Pusat', '1970/08/01', '2006/07/07'),
('IP06002', 'Amin', 'Jln. Bungur sari v No, 178, bandung', '1977/05/03', '2006/07/06'),
('IP06003', 'Yusuf', 'Jln. Yosodpuro 15, surabaya', '1973/08/09', '2006/07/08'),
('IP06004', 'Alyssa', 'Jln. Cendana No. 6 Bandung', '1983/02/14', '2007/01/05'),
('IP06005', 'Maulana', 'Jln. Ampera Raya No 1', '1985/10/10', '2007/02/05'),
('IP06006', 'Afika', 'Jln. Pejaten Barat No 6A', '1987/03/09', '2007/06/09'),
('IP06007', 'James', 'Jln. Padjadjaran No. 111, bandung', '1988/05/19', '2007/06/09'),
('IP06008', 'Oktavanus', 'Jln. Gajah Mada 101. Semarang', '1988/10/07', '2008/08/08'),
('IP06009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta selatan', '1988/01/20', '2008/11/11'),
('IP06010', 'Raisa', 'Jln. Nangka Jakarta selatan', '1989/12/29', '2009/02/09');


insert into cuti_karyawan (nomor_induk, tanggal_mulai, lama_cuti, keterangan)
values 
('IP06001', '2012/2/1', 3, 'Acara Keluarga'),
('IP06001', '2012/2/13', 4, 'Anak Sakit'),
('IP06007', '2012/2/15', 2, 'Nenek Sakit'),
('IP06003', '2012/2/17', 1, 'Mendaftar Sekola Anak'),
('IP06006', '2012/2/20', 5, 'Menikah'),
('IP06004', '2012/2/27', 1, 'Imunisasi Anak');

--1
select * from karyawan 
order by tanggal_masuk
limit 3

--2
select k.nomor_induk, k.nama, ck.tanggal_mulai, ck.lama_cuti, ck.keterangan, (tanggal_mulai+lama_cuti) as hitung
from karyawan k 
join cuti_karyawan ck on ck.nomor_induk = k.nomor_induk 
where ck.tanggal_mulai < '2012\02\16' and (ck.tanggal_mulai + ck.lama_cuti) >= '2012\02\16'


---yang sudah benar
select k.nomor_induk, k.nama, ck.tanggal_mulai, ck.lama_cuti, ck.keterangan, (tanggal_mulai+(lama_cuti - 1)) as hitung
from karyawan k 
join cuti_karyawan ck on ck.nomor_induk = k.nomor_induk 
where ck.tanggal_mulai < '2012\02\16' and (ck.tanggal_mulai + (ck.lama_cuti - 1)) >= '2012\02\16'

---------
select * from cuti_karyawan ck 
select (tanggal_mulai+lama_cuti) as hitung from cuti_karyawan ck  
--------------


--3
select k.nomor_induk, k.nama, count(lama_cuti) as jumlah 
from cuti_karyawan ck 
join karyawan k on ck.nomor_induk = k.nomor_induk  
group by k.nomor_induk, k.nama
having count(lama_cuti) > 1

--4
--fungsi coalesce mengagantikan null dengan statement terakhir(default nya di statement akhir), statement dipisahkan dengan tanda koma(,)
select k.nomor_induk, k.nama, coalesce(ck.lama_cuti, 0) as lama_cuti, coalesce(12-sum(ck.lama_cuti), 12) as sisa_cuti
from karyawan k 
left join cuti_karyawan ck on k.nomor_induk = ck.nomor_induk
group by k.nomor_induk, k.nama, ck.lama_cuti
order by sisa_cuti desc

--5
select concat(nama, ' ', nomor_induk) as nama_nomorInduk
from karyawan
order by nama_nomorInduk
