package latihan;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class TarifParkir {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan1 = new Scanner(System.in);

		String masuk = namaToAngka(scan1.nextLine());
		String keluar = namaToAngka(scan1.nextLine());

//		System.out.println(masuk);
//		System.out.println(keluar);

		String wParkir = lamaParkir(masuk, keluar);
//		System.out.println(wParkir);

		System.out.println(tarifParkir(wParkir));

//		System.out.println(masuk);
//		String masuk = "28 Januari 2020 07:30:34";
//		String keluar = "28 Januari 2020 20:03:34";
//		

	}

	public static String namaToAngka(String input) {
		String result = "";

		String[] temp = input.toLowerCase().split(" ");

		if (temp[1].equals("januari")) {
			result += temp[2] + "-01-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("februari")) {
			result += temp[2] + "-02-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("maret")) {
			result += temp[2] + "-03-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("april")) {
			result += temp[2] + "-04-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("mei")) {
			result += temp[2] + "-05-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("juni")) {
			result += temp[2] + "-06-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("juli")) {
			result += temp[2] + "-07-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("agustus")) {
			result += temp[2] + "-08-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("september")) {
			result += temp[2] + "-09-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("oktober")) {
			result += temp[2] + "-10-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("november")) {
			result += temp[2] + "-11-" + temp[0] + "T" + temp[3];
		} else if (temp[1].equals("desember")) {
			result += temp[2] + "-12-" + temp[0] + "T" + temp[3];
		}

		return result;
	}

	public static String lamaParkir(String masuk, String keluar) {

		String result = "";

		LocalDateTime waktuMasuk = LocalDateTime.parse(masuk);
		LocalDateTime waktuKeluar = LocalDateTime.parse(keluar);

		long days = ChronoUnit.DAYS.between(waktuMasuk, waktuKeluar);
		long hours = ChronoUnit.HOURS.between(waktuMasuk, waktuKeluar);
		long hoursFix = hours - (24 * days);
		long minutes = ChronoUnit.MINUTES.between(waktuMasuk, waktuKeluar);
		long minutesFix = minutes - (60 * hours);

		result += days + " hari " + hoursFix + " jam " + minutesFix + " minutes";

		return result;

	}

	public static String tarifParkir(String tarif) {
		String result = "";

		String[] arrTarif = tarif.split(" ");

		long temp = 0;

		if (Integer.parseInt(arrTarif[0]) >= 1) {
			temp = temp + (Integer.parseInt(arrTarif[0]) * 15000);
		} else if (Integer.parseInt(arrTarif[2]) >= 7 && Integer.parseInt(arrTarif[4]) >= 1
				|| Integer.parseInt(arrTarif[2]) >= 8) {
			temp = temp + 8000;
		} else if (Integer.parseInt(arrTarif[2]) < 8) {
			temp = temp + (Integer.parseInt(arrTarif[2]) * 1000);
		}

		result += temp;

		return result;
	}

}
