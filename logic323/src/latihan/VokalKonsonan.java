package latihan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class VokalKonsonan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Masukkan teks : ");
		String input = scan.nextLine().toLowerCase();
		
		vokalKonsonan(input);

	}

	public static void vokalKonsonan(String input) {

		String n = input.replace(" ", "");
		char[] arr = n.toCharArray();
//		Arrays.sort(arr);
		char temp = ' ';

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length - i - 1; j++) { // buble short
				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;

				}
			}

		}

		String vokal = "";

		String konsonan = "";

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == 'a' || arr[i] == 'i' || arr[i] == 'u' || arr[i] == 'e' || arr[i] == 'o') {
				vokal += arr[i];
			} else {
				konsonan += arr[i];
			}
		}

		System.out.println(vokal);
		System.out.println(konsonan);

		String baru = "";
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] == arr[i - 1]) {
				baru += arr[i];
			} else {
				baru += "-" + arr[i];
			}
		}

		System.out.println(baru);
	}

}
