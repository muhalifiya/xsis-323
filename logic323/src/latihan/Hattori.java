package latihan;

import java.util.Scanner;

public class Hattori {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Input :");
		String c = input.nextLine().replace(" ", "");

		int gunung = 0;
		int lembah = 0;

		int mdpl = 0;

		char[] arrC = c.toCharArray();

		for (int i = 0; i < arrC.length; i++) {
			if (arrC[i] == 'N') {
				mdpl += 1;
				if (mdpl == 0) {
					lembah += 1;
				}
			} else if (arrC[i] == 'T') {
				mdpl -= 1;
				if (mdpl == 0) {
					gunung += 1;
				}
			}
		}

		System.out.println("Gunung = " + gunung);
		System.out.println("Lembah = " + lembah);
	}

}
