package latihan;

import java.util.Arrays;
import java.util.Scanner;

public class Porsi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		totalPorsi(scan.nextLine());
	}

	public static void totalPorsi(String input) {

		String[] arrInput = input.split(", ");

		double lakiLaki = 0;
		double perempuan = 0;
		double anakAnak = 0;
		double balita = 0;
		double porsi = 0;

		for (int i = 0; i < arrInput.length; i++) {

			String temp = arrInput[i];
//			System.out.println(temp);
			String[] arrTemp = temp.split(" ");
			int orang = Integer.parseInt(arrTemp[arrTemp.length - 1]);

			for (int j = 0; j < 1; j++) {
				if (arrTemp[j].equals("laki-laki")) {
					lakiLaki += orang;
//					System.out.println(lakiLaki);
				} else if (arrTemp[j].equals("perempuan")) {
					perempuan += orang;
//					System.out.println(perempuan);
				} else if (arrTemp[j].equals("anak-anak")) {
					anakAnak += orang;
//					System.out.println(anakAnak);
				} else if (arrTemp[j].equals("balita")) {
					balita += orang;
//					System.out.println(balita);
				}
			}
		}

		String result = "";
		if ((lakiLaki + perempuan + anakAnak + balita) > 5 && (lakiLaki + perempuan + anakAnak + balita) % 2 == 1) {
			porsi = (lakiLaki * 2) + (perempuan * 2) + (anakAnak * 0.5) + (balita * 1);
			result += porsi;
		} else {
			porsi = (lakiLaki * 2) + (perempuan * 1) + (anakAnak * 0.5) + (balita * 1);
			result += porsi;
		}

		System.out.println(result.substring(0, result.length()-2) + " porsi");

	}

}
