package latihan;

import java.util.Arrays;
import java.util.Scanner;

public class Penjumlahan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		System.out.println("panjang deret N:");
		int n = input.nextInt();

		int[] diKurang = new int[n];
//		int kelipatan3 = 3;
		int[] diBagi = new int[n];
//		int kelipatan4 = 4;

		for (int i = 0; i < n; i++) { //

			diKurang[i] = (i * 3) - 1;
			diBagi[i] = (i * 4) / 2;

		}

		System.out.println(Arrays.toString(diKurang));
		System.out.println(Arrays.toString(diBagi));

		int[] tambah = new int[n];
		String hasilTambah = "";

		for (int i = 0; i < n; i++) {
			tambah[i] += diKurang[i] + diBagi[i];
			hasilTambah += tambah[i] + " ";
		}

		System.out.println(Arrays.toString(tambah));
		System.out.println(hasilTambah);

	}

}
