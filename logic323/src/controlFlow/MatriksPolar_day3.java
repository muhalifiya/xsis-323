package controlFlow;

public class MatriksPolar_day3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		nomor1(7);

//		nomor2(7);
//
//		System.out.println();
//
//		nomor3(5);
//		
//		System.out.println();
//		coba(5);

	}

	public static int nomor1(int n) {
		int a = 2;
		int b = 3;
		int hasil = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (j == n / 2 && (i == n - n || i == n - 1)
						|| (i == n - (n - 1) || i == n - 2) && (j == n - n || j == n - 1)) {
					System.out.print(b + " ");
				} else if (i == n - n || i == n - 1) {
					System.out.print(a + " ");
					hasil = a + b;
					a = hasil;
				} else if (j == n - 1 || j == n - n) {
					System.out.print(a);
					hasil = a + b;
					a = hasil;
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();

		}
		return n;
	}

	public static int nomor2(int n) {
		int a = 2;
		int b = 3;
		int hasil = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == j && i == n / 2 || j == n - 1 && i == n - (n - 1)) {
					System.out.print(b);
				} else if ((i + j) == n - 1 && i != n / 2) {
					System.out.print(a + " ");
					hasil = a + b;
					a = hasil;
				} else if (j == n - 1 || i == n - 1) {
					System.out.print(a + " ");
					hasil = a + b;
					a = hasil;
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();

		}
		return n;
	}

	public static int nomor3(int n) {
		int hasil = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == n - n || i == n - 1) {
					System.out.print(hasil + " ");
					hasil++;
				} else if (j == n - 1 || j == n - n) {
					System.out.print(hasil);
					hasil++;
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();

		}
		return n;
	}
	
	public static int coba(int n) {
		int a = 2;
		int b = 3;
		int hasil = 0;
		for (int i = 5; i > 1; i--) {
			for (int j = 1; j < n; j++) {
				if((i+j)==n+1) {
					System.out.print(a + " ");
					hasil = a+b;
					a = hasil;
				}else {
					System.out.print(" ");
				}
				
			}
			System.out.println( i + "======");

		}
		return n;
	}

}
