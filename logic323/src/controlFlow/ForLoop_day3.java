package controlFlow;

public class ForLoop_day3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (int i = 0; i < 5; i++) {
			System.out.println(i + ", ");
		}

		System.out.println();

		for (int i = 1; i <= 5; i++) {
			System.out.println(i + ", ");
		}

		System.out.println();

		for (int i = 0; i <= 5; i += 1) {
			System.out.println(i + ", ");
		}

		System.out.println();

		for (int i = 0; i <= 5; i += 2) {
			System.out.println(i + ", ");
		}

		// 20 25 30 35 40 45
		System.out.println();

		for (int i = 20; i <= 45; i += 5) {
			System.out.println(i + ", ");
		}

		System.out.println();

		for (int i = 0; i <= 5; i++) {
			System.out.println(i + ", ");
		}

		System.out.println();

		for (int i = 0; i <= 5; i++) {
			System.out.println("Hello...");
		}

		System.out.println();

		for (int i = 5; i >= 1; i--) {
			System.out.println(i + ", ");
		}

		System.out.println();

		// tahun 1990-2000
		for (int i = 1990; i <= 2000; i++) {
			System.out.println(i + ", ");
		}

		System.out.println();

		// tahun 1990-2000 tahun kabisat
		for (int i = 1990; i <= 2000; i += 4) {
			System.out.println(i + ", ");
		}

		System.out.println();

		int tahun = 1990;
		for (int i = 1; i <= 11; i++) {
			System.out.println(tahun + ", ");
			tahun++;
		}

		System.out.println();

		// 1 + 2 + 3 + 4 + 5 = 15
		int result = 0;
		for (int i = 1; i <= 5; i++) {
			result += i;
		}

		System.out.println(result);

		System.out.println();

		result = 1;
		for (int i = 1; i <= 5; i++) {
			result *= i;
		}
		System.out.println(result);
		System.out.println();

		// print bilangan ganjil 1 - 25
		for (int i = 1; i <= 25; i++) {
			if (i % 2 != 0) {
				System.out.println(i + ",");
			}

		}

		for (int i = 1; i <= 25; i += 2) {
			System.out.println(i + ",");

		}

		System.out.println();

		/*
		 * 1+2 = 3 3+4 = 7 5+6 = 11
		 */

		for (int i = 1; i <= 5; i++) {
			System.out.println(i + " + " + (i + 1) + " = " + (i + (i + 1)));
			i++;

		}

		/*
		 * 100 100 300 1500 10500
		 */
		int a=100;
		int hasil = 0;
		for (int i = 1; i <= 7; i+=2) {
			hasil = i * a;
			System.out.print(hasil + " ");
			a = hasil;

		}

	}

}
