package controlFlow;

public class NestedFor_day3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (int i = 1; i <= 3; i++) {
			for (int j = 1; j <= 3; j++) {
				System.out.print("[" + i + "][" + j + "] ");
			}
			System.out.println();
		}

		System.out.println();

		/*
		 * 0 1 2 
		 * 0 1 2 
		 * 0 1 2
		 * 
		 */
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}

		System.out.println();

		int a = 1;
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {

				System.out.print(a + " ");
				a += 2;
			}
			System.out.println();
		}
		System.out.println();

		/*
		 * 1 0 0
		 * 0 1 0 
		 * 0 0 1
		 */

		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {

				if (i == j) {
					System.out.print(1);
				} else {
					System.out.print(0);
				}

			}
			System.out.println();
		}
		System.out.println();

		/*
		 *      j j j 
		 *      0 1 2 
		 * i 0 |1 0 0 
		 * i 1 |* 1 0 
		 * i 2 |* * 1
		 * 
		 */
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {

				if (i == j) {
					System.out.print(1);
				} else if (i >= 1 && j <= 1) {
					System.out.print("*");
				} else {
					System.out.print(0);
				}

			}
			System.out.println();
		}

		System.out.println();

		/*
		 * 0 0 1 
		 * 0 1 0 
		 * 1 0 0
		 */
		int n = 3;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {

				if ((i + j) == n - 1) {
					if (j == n - 1) {
						System.out.print(1);
					} else {
						System.out.print(1 + " ");
					}
				} else {
					if (j == n - 1) {
						System.out.print(0);
					} else {
						System.out.print(0 + " ");
					}
				}

			}
			System.out.println();
		}

	}

}
