package controlFlow;

public class MatrixPola {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		nomor1();
		System.out.println();
		System.out.println();

		nomor2();
		System.out.println();
		System.out.println();

		nomor3();

	}

	public static void nomor1() {
		int a = 2;
		int d = 56;
		int c = 44;
		int b = 20;
		for (int i = 1; i <= 7; i++) {
			for (int j = 1; j <= 7; j++) {
				if (i == 1 && j != 4) {
					System.out.print(a + " ");
					a += 3;

				} else if (i == 7 && j != 4) {
					System.out.print(c + " ");
					c -= 3;

				} else if (j == 1 && i != 2 && i != 6) {
					System.out.print(d + " ");
					d -= 3;

				} else if (j == 7 && i != 2 && i != 6) {
					System.out.print(b + " ");
					b += 3;
				} else if (((j == 1) && (i != 2 || i != 6)) || ((j == 7) && (i != 2 || i != 6))
						|| (j == 4 && (i == 1 || i == 7))) {
					System.out.print("3  ");
				} else {
					System.out.print("   ");
				}

			}
			System.out.println();

		}
	}

	public static void nomor2() {
		int a = 17;
		int b = 20;
		int c = 44;
		int e = 3;
		int f = 81;

		for (int i = 1; i <= 7; i++) {
			for (int j = 1; j <= 7; j++) {
				if (i == 4 && j == 4) {
					System.out.print(e + " ");
				} else if (i == 7 && j == 4) {
					System.out.print(f + " ");
				} else if ((i + j) == 7 + 1) {
					System.out.print(a + " ");
					a -= 3;
				} else if (i == 7 && j != 1) {
					System.out.print(c + " ");
					c -= 3;
				} else if (j == 7 && i == 2) {
					System.out.print(e + " ");
				} else if (j == 7 && i != 1 || j == 7 && i != 2 || j == 7 && i != 7) {
					System.out.print(b);
					b += 3;
				} else {
					System.out.print("   ");
				}
			}
			System.out.println();
		}

	}

	public static void nomor3() {
		int a = 0;
		int b = 7;
		int c = 17;
		int d = 23;

		for (int i = 1; i <= 7; i++) {
			for (int j = 1; j <= 7; j++) {
				if (i == 1) {
					System.out.print(a + "  ");
					a++;
				} else if (j == 1) {
					System.out.print(d + " ");
					d--;
				} else if (j == 7) {
					System.out.print(b + " ");
					b++;
				} else if (i == 7 && (j != 1 || j != 7)) {
					System.out.print(c + " ");
					c--;
				}else {
					System.out.print("   ");
				}
			}
			System.out.println();
		}
	}
}
