package controlFlow;

public class WhileDoWhile_day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// for component
		// 1. initialization (i=0; j=1; k=0)
		// 2. condition expression (<, >, <=, >=)
		// 3. expression list(increment, decrement, compound assigment)

		int i = 1;
		while (i <= 5) {// didalam kurung merupakan kondisi ekspresi
			// while = "selama i <= 5 putar/loop"
			System.out.print(i + ",");
			i++;

		}
		System.out.println();

		do {// lakukan/putar/loop
			System.out.println(i + "Xsis");
			i++;
		} while (i <= 5);// selama i <= 5

		System.out.println("a");

		faktorialWhile(5);

		faktorialDoWhile(5);

	}

	public static int faktorialWhile(int a) {
		int faktorial = 1;
		int i = 0;

		while (i <= a) {
			if (i == 0) {
				System.out.println(i + "! = " + faktorial); 
			} else {
				faktorial = faktorial * i;
				System.out.println(i + "! = " + faktorial);

			}

			i++;

		}
		System.out.println();

		return a;

	}

	public static int faktorialDoWhile(int a) {
		int faktorial = 1;
		int i = 0;

		do {
			if (i == 0) {
				System.out.println(i + "! = " + faktorial);
			} else {
				faktorial = faktorial * i;
				System.out.println(i + "! = " + faktorial);

			}
			i++;
		} while (i <= 5);

		return a;

	}

}
