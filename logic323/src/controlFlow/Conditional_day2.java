package controlFlow;

import java.util.Scanner;

public class Conditional_day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int angka1 = 99;
		int angka2 = 100;

		if (angka2 < angka1) { // (angka2 < angka1) -> kondisi
			System.out.println("angka1 lebih besar"); // -> statement
		} else {
			System.out.println("angka2 lebih besar");
		}

		String a = "Xsis";
		String b = "Academy";

		if (a == b) {
			System.out.println("String sama");
		} else {
			System.out.println("Tidak sama");
		}

		int angka = randomNumber(50, 99);

		Scanner scan = new Scanner(System.in);
		System.out.print("Silahkan tebak angka : ");
		int tebak = scan.nextInt();

		if (tebak > angka) {
			System.out.println("Tebakan lebih besar");
		} else if (tebak < angka) {
			System.out.println("Tebakan lebih kecil");
		} else {
			System.out.println("Tepat");
		}

		System.out.println("Angka yang harus ditebak adalah : " + angka);

		
		
		String name = "Logan";
		String salutation = "";
		Boolean isMale = true;
		if (isMale) {
			salutation = "Mr. ";
		} else {
			salutation = "Ms. ";
		}
		System.out.println("Good evening " + salutation + name);
		
		String salut = (isMale ? "Mr. ": "Ms. ");
		System.out.println("Good evening " + salut + name);

	}

	public static int randomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);

	}

}
