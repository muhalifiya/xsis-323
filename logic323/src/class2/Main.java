package class2;

public class Main {

	public static void main(String[] args) {
		
		//class abstract isi nya merupakan deklarasi" class method
		// bedanya dengan polymorphism untuk class utamanya(parent) tidak dapat di instance/instansiasi 
		//Shape SHAPE = new Shape(); 
		
		Rectangle rectangle = new Rectangle();
		Circle circle = new Circle();
		
		rectangle.draw();
		rectangle.shape();
		
		circle.draw();
		circle.shape();
		
		System.out.println();
		
		Cat cat = new Cat();
		Horse horse = new Horse();
		
		cat.animalSound();
		cat.sleep();
		
		horse.animalSound();
		horse.sleep();

	}

}
