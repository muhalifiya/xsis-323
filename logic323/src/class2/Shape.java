package class2;

public abstract class Shape {
	public abstract void draw ();
	
	
	//bukan constructor karena nama method berbeda dengan nama class
	public void shape() {
		System.out.println("Draw a shape");	
	}
	
}
