package class2;

public class Race {
	
	//kalau private hanya berlaku diclass sendiri tidak dapat diakses walau pada package yang sama
	
	//jika tidak diberi akses modifier public maka hanya berlaku pada package yang sama
	//String region;
	
	public String region;// karena berada pada package yang berbeda harus diberi akses modifier public pada property nya;
	public String origin;
	
	
	//constructor
	public Race() {//nama method = nama class <- adalah constructor
		region = "Earth";
	}
	
	public Race(String reg, String ori) {
		region = reg;
		origin = ori;
	}

	public String getOrigin() {
		return origin + ", " + region;
	}
	
	//polymorphism	
	public void uniqVehicle() {
		System.out.println("Kendaraan unik");
	}
}
