package ft1;

import java.util.Scanner;

public class Soal10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Kalimat :");
		String Kalimat = input.nextLine().toLowerCase();
		
		String konsonan = "bcdfghjklmnpqrstvwxyz";
		String vokal = "aiueo";

		int hitung = 0;
		
		for (int i = 0; i < Kalimat.length(); i++) {
			if (konsonan.contains(Kalimat.charAt(i)+ "") && vokal.contains(Kalimat.charAt(i + 1) + "")
					&& i != Kalimat.length() - 1) {
				hitung++;
			}
		}

		System.out.print(hitung);

	}

}
