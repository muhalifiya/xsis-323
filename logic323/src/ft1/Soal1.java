package ft1;

import java.util.Arrays;
import java.util.Scanner;

public class Soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Buatlah format penjumlahan seperti contoh di bawah ini,
		 * 
		 * Input: n (deret angka) Output: penjumlahan seperti contoh Constraint: angka
		 * bisa berupa bilangan bulat positif, bilangan bulat negatif, ataupun desimal
		 * 
		 * Example 1: n = 4 1 3 output: 4 4 + 1 = 5 4 + 1 + 3 = 8
		 * 
		 * Example 2: n = 2 2 3 0 8 
		 * output: 2 
		 * 2 + 2 = 4 
		 * 2 + 2 + 3 = 7 
		 * 2 + 2 + 3 + 0 = 7
		 * 2 + 2 + 3 + 0 + 8 = 15
		 * 
		 */
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Angka");
		String angka = input.nextLine();

		String[] deretAngka = angka.split(" ");

		int temp = 0;
		
		for (int i = 0; i < deretAngka.length; i++) {
			
			temp = temp + Integer.parseInt(deretAngka[i]);
			System.out.println(temp);
			
		}

	}

}
