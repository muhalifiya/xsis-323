package logic323.variable;

public class Scope_day2 {
	
	//oop adalah reusable artinya bisa dipakai ulang 
	
	//variable global
	public static int a;
	public static String str;
	
	//private hanya bisa dibaca dalam class ini
	private int x = 3;
	private int y = 7;

	public static void main(String[] args) { //->method
		// TODO Auto-generated method stub

		/*
		 * Semua variable yang berada didalam method 
		 * juga didalam method main sifat nya adalah lokal
		 * hanya berlaku di dalam mthod itu sendiri.
		 */
		
		int b = 1;
		a = 3;
		int hasil = a + b * 2;
		System.out.println(hasil);
		
		System.out.println();
		
		float hasil2 = Pembagian(hasil);
		System.out.println(hasil2);
		
		System.out.println();
		
		System.out.println(peran());
		String hasil3 = peran() + " - Increadable Hulk"; // peran() -> merupakan fungsi
		System.out.println(hasil3);
		
		System.out.println();
		
		//instansiasi
		Scope_day2 scope = new Scope_day2();
		System.out.println(scope.Pengurangan());
		
		
	}
	
	public static float Pembagian(int x) {
		//int a = 2; -> variable lokal; variable lokal akan dibaca duluan dari pada variable global
//		int a = 2;
		return  (float) a /  (float) x; // (float) -> mengkonfersi int ke float
	}
	
	public static String peran() {
		str = "David Banner"; // str -> variable global
		return str;
		
	}
	
	public int Pengurangan() {
		int x = 6;
		a = 20;
		return (this.x - this.y) * x + a; //this berarti penggunaan variable global kalau di oop this disebut property
//		int a =21;
//		return (this.x - this.y)*x+this.a+ a;
		
	}

}
