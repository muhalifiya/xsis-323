package logic323.variable;

public class ArithmeticOperator_day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int a = 5;
		int b = 2;
		
		int sisaBagi = a % b; //operator Modulus/sisa pembagian
		System.out.println(sisaBagi);
		
		//operator increment
		int inc = a++; // | a = a + 1 (Belum dilakukan penjumlahan)
		System.out.println(inc);
		int inc_a = a;
		System.out.println(inc_a);
		int inc2 = ++a; // | a + 1 = a
		System.out.println(inc2);
		
		System.out.println();
		
		
		//operator decrement
		int dec = b--; // b = b - 1 (Belum dilakukan pengurangan)
		System.out.println(dec);
		int dec2 = b;
		System.out.println(dec2);
		int dec3 = --b; // b - 1 = b
		System.out.println(dec3);
		
		System.out.println();
		
		int c = 7;
		
		//Compound assignment
		int com = c += 1; // c = c + 1 (Sudah dilakukan penjumlahan)
		System.out.println(com);
		int com2 = c += 2; // c = c + 1
		System.out.println(com2);
		
		int d = 9;
		int com3 = d -= 5;
		System.out.println(com3);
		int com4 = d -= 5;
		System.out.println(com4);
		
		int e = 2;
		int com5 = e *= 2;
		System.out.println(com5);
		int com6 = e *= 2;
		System.out.println(com6);
		
		int f = 10;
		int com7 = f /= 2;
		System.out.println(com7);
		int com8 = f /= 2;
		System.out.println(com8);
		
		float g = 20;
		float com9 = g %= 5;
		System.out.println(com9);
		
		
		
	}

}
