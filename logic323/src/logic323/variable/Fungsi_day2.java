package logic323.variable;

public class Fungsi_day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		// f(x) = 3
		// <- matematika == -> pemrograman fungsional
		// f sebagai nama funsi == sebagai nama fungsi
		// x sebagai variable == sebagai parameter
		// 3 hasilakhir == return value
		
//		System.out.println(penjumlahan(5));
		float hasil = penjumlahan(5);
		System.out.println(hasil);
		float hasil2 = penjumlahan(hasil, 5);
		System.out.println(hasil2);
//		float hasil3 = penjumlahan(hasil);
//		System.out.println(hasil3);
	}

	//fungsi tidak didalam main
	//nama funsi yang sama di java diperbolehkan namanya overloading cuma pembedanya parameter nya baik banyak nya atau tipe datanya
	
	public static float penjumlahan(float x) {
		return x;
	}
	
	public static float penjumlahan(float x, float y) {
		return x + y;
	}
	
//	public static int penjumlahan(int a) {
//		return a + 9;
//	}
}
