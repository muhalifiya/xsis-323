package logic323.variable;

public class TernaryOperator_day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a = 20;
		int b = 30;
		
		int ternary = (a > b ? a : b);
		System.out.println(a);
		System.out.println(b);
		System.out.println(ternary);
		
		int ternary2 = (a == b ? 100 : 50);
		System.out.println(ternary2);
		
		int ternary3 = (a != b ? 100 : 50);
		System.out.println(ternary3);
		
		String str1 = "Xsis";
		String str2 = "Academy";
		boolean ternary4 = (str1 == str2 ? true : false);
		System.out.println(ternary4);
		boolean ternary5 = (str1 != str2 ? true : false);
		System.out.println(ternary5);
		
	}

}
