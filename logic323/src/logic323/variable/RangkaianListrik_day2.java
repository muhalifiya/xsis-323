package logic323.variable;

public class RangkaianListrik_day2 {
	public static void main(String[] args) {
		
		

//		System.out.println("Arus yang melewati rangkaian seri R1 = "+arusR1(20)+" A");
//		System.out.println("Arus yang melewati seluruh rangkaian seri = "+arusMengalir(20,3200,1000,500)+" A");
		
	arusR1(20);	
	}
	
	
	public static float arusR1(float r1) {
		float V = 9f;
		float I = V/r1;
		
		return I; 		
	
	}
	
	public static float rangkaianSeri(float r1, float r2) {
		return r1+r2;
	}
	
	public static float rangkaianParalel(float r3, float r4) {
		return r3*r4/(r3+r4);
	}
	
	public static float arusMengalir(float r1, float r2, float r3, float r4) {
		float V = 9f;
		float I =  V/(rangkaianSeri(r1, r2) + rangkaianParalel(r3, r4));
		
		return I;
		
	}
	

}
