package logic323.variable; //logic323.variable -> Domain

public class Variable1_day1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Baris 1");
		System.out.println("Baris 2");
		System.out.println("Baris 3");
		
		
		System.out.println();
		
		float E;
		float m;
		float c= 299792458; //reference by value
		
		m = 51.3f; //sebagai penanda untuk mendefinisikan float
		E = m * (c * c);
		System.out.println(E);
		System.out.println();
		
		//menghitung keliling lingkaran (pi * 2 * r)
		
		int r = 7;
		float pi;
		float keliling;
		double pi2 = 22/7d;
		
		pi = 22/7f;
		keliling = (float) pi2 * 2 * r; //convert dari double ke float
		System.out.println(pi);
		System.out.println(pi2);
		System.out.println(keliling);
		System.out.println();
		
		float V = 7.0f; //tegangan 
		float I = 3.2f; //arus
		float R; //hambatan
		
		R = V / I;
		System.out.println(R);
		V = I * R;
		System.out.println("Voltage : " + V);
		I = V / R;
		System.out.println(I);
		
		System.out.println();
		
		
		String str = "Hello ";
		String name = " DIta";
		String greeting = str + " " + name;
		System.out.println(greeting);
		String greet2 = str + "\t" + name;
		System.out.println(greet2);
		System.out.println();
		
		
		char ch1 = 'A';//karakter harus 1 huruf
		char ch2 = 'B';
		System.out.println(ch1);
		System.out.println(ch2);
		System.out.println(ch1 + ch2);//menjadi ascii
		System.out.println(ch1 +""+ ch2); //"" mengubah char menjadi string
		System.out.println(0 + ch1);
		System.out.println(0 + ch2);
		
		//method ada dua bagian fungsi dan prosedur/prosedural
		//prosedur tidak menghasilkan return tapi dapat menjalankan
		//fungsi
		
	}

}
