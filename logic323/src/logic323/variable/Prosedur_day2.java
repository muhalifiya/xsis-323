package logic323.variable;

public class Prosedur_day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//prosedur merupakan proses yang menghasilkan keluaran
		//ciri void(kosong/tidak berlaku/diabaikan) menandakan tipe data diabaikan tidak perlu membuat return value
		Hello("Agung");
		Hello("Denis");
		
		//tidak bisa seperti ini karena dia hanya keluaran sistem
		//String greeting = Hello("Peter");
		//String greeting = Hello("Peter") + Hello("Peter");
		
		System.out.println();
		
		Penjumlahan(5, 9);
		
		System.out.println();
		
		float luas = luasLingkaran(7);
		System.out.println(luas);
		
		float volume = volumeTabung(7, 10);
		System.out.println(volume);
		
		System.out.println();
		
		printLingkaran(7, 10);
		printLingkaran(3, 5);
		
	}
	
	public static void Hello(String name) {
	 System.out.println("Hello, " + name);
	 System.out.println("Welcome to Xsis Academy");
	 
	//void tidak perlu pakai return karena return nya void tidak ada(akan diabaikan)
	 //return 0;
		
	}
	
	public static void Penjumlahan(int a, int b) {
		System.out.println(a + b);
	}
	
	
	
	public static float luasLingkaran(float r) { //fungsi ini menjadi mandatory bagi fungsi volumeTabung
		return 3.14f * (r*r);
	
	}
	
	public static float volumeTabung(float r, float tinggi) {
		return luasLingkaran(r) * tinggi;
		
	}
	
	public static void printLingkaran (float r, float tinggi) {
		float luas = luasLingkaran(r);
		float volume = volumeTabung(r, tinggi);
		System.out.println("Luas lingkaran :" + luas);
		System.out.println("Jika tinggi tabung nya adalah :" + tinggi);
		System.out.println("Volume tabung nya adalah :" + volume);
	}
	

}
