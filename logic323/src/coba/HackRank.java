package coba;

public class HackRank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String s = "91011";
		String result = "";
	    boolean isValid = false;
	     
	    //99100 5/2 = 2
	    for(int i = 0; i < s.length()/2; i++){ //1<2
	        result = s.substring(0, i+1); 
	        long num = Long.parseLong(result); 
	        String finalResult = result; 
	    
	        while(finalResult.length() < s.length()){ 
	        finalResult += Long.toString(++num);  
	        }

	        if(s.equals(finalResult)){ 
	        isValid = true;
	        break;
	        }   
	    } 

	    System.out.println(isValid ? "YES " + result : "NO");
	}

}
