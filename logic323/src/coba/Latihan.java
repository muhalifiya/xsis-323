package coba;

import java.util.Arrays;
import java.util.Scanner;

public class Latihan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		kelipatan3Genap();

		int[] x = kelipatan3Genap();
		System.out.println(Arrays.toString(x));

		System.out.println(median(x));

	}

	public static int[] kelipatan3Genap() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Masukkan panjang kelipatan 3 : ");

		int input = scan.nextInt();

		int[] kelipatan3 = new int[input];

		int awal = 3;

		String genapKelipatan3 = "";

		for (int i = 0; i < kelipatan3.length; i++) {
			kelipatan3[i] += awal;
			awal += 3;

			if (kelipatan3[i] % 2 == 0) {
				genapKelipatan3 += kelipatan3[i] + " ";
			}

		}

		System.out.println("==="+genapKelipatan3);

		String[] gK3 = genapKelipatan3.split(" ");
		int[] median = new int[gK3.length];

		System.out.println("---"+gK3.length);

//		String hasil = "";

		for (int i = 0; i < gK3.length; i++) {
			median[i] = Integer.parseInt(gK3[i]);

		}
		System.out.println(Arrays.toString(median));

		return median;

//		if(gK3.length % 2 == 0) {
//			hasil += (median[median.length/2] + median[(median.length/2)-1]) / 2;
//		} else {
//			hasil += median[median.length/2];
//		}
//		

//		String hasil = genapKelipatan3.substring(0, genapKelipatan3.length()-2);
//		System.out.println(hasil);

	}

	public static String median(int[] median) {
		String hasil = "";

		if (median.length % 2 == 0) {
			hasil += (median[median.length / 2] + median[(median.length / 2) - 1]) / 2;
		} else {
			hasil += median[median.length / 2];
		}

		return hasil;

	}

}
