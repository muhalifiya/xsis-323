package coba;

import java.util.Arrays;
import java.util.Scanner;

public class Fibonaci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Input = ");
		int x = input.nextInt();

		int[] arrFibonaci = new int[x];

		String angka = "";

		for (int i = 0; i < x; i++) {
			if (i <= 1) {
				arrFibonaci[i] = 1;
			} else {
				arrFibonaci[i] = arrFibonaci[i - 1] + arrFibonaci[i - 2];
			}

		}

		for (int i = 0; i < x; i++) {
			if (arrFibonaci[i] % 2 == 0) {
				angka += arrFibonaci[i] + " ";
			} 
		}

		System.out.println(Arrays.toString(arrFibonaci));
		System.out.println(angka.trim());
	}

}
