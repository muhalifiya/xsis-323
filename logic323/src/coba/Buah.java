package coba;

import java.util.Scanner;

public class Buah {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		hargaBuah();
	}
	
	public static void hargaBuah() {
		/*
		 * Berikut ini kata kunci nama buah beserta harganya Semangka seharga 30 ribu
		 * per kg Pir seharga 10 ribu per kg
		 * 
		 * Berapa harga buah yang lain ?
		 * 
		 * Input: nama buah Output: harga buah per kg
		 * 
		 */
		Scanner scan = new Scanner(System.in);
		System.out.print("Masukkan nama buah : ");
		String namaBuah = scan.nextLine().toLowerCase().replace(" ", "");

		char[] arrBuah = namaBuah.toCharArray();

		int total = 0;
		for (int i = 0; i < arrBuah.length; i++) {
			if (arrBuah[i] == 'a' || arrBuah[i] == 'i' || arrBuah[i] == 'u' || arrBuah[i] == 'e' || arrBuah[i] == 'o') {
				total += 10;
			}
		}

		System.out.println("Harga Buah " + namaBuah + " per Kg : " + total + "Ribu");

	}

}
