package exception;

import java.util.Arrays;

public class TryCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// contoh div by zero
		float x = 15, y = 0, hasil;
		// tanpa try-catch blok
		hasil = x / y;
		System.out.println(hasil);
		// dengan try-catch blok
		try {
			hasil = x / y;
			System.out.println(hasil);
		} catch (ArithmeticException e) {
			String mssg = e.getMessage();

		}

		int A[] = new int[3];
		try {
			A[0] = 3;
			A[1] = 5;
			A[3] = 1; // penyebab exception
			A[2] = 7;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());// -> menampilkan exception message(error message)
//			A[0] = 0;
//			A[2] = 9;
		}
		System.out.println(Arrays.toString(A));

		System.out.println();

		try {
			A[0] = 3;
			A[1] = 5;
			A[3] = 1; // penyebab exception
			A[2] = 7;
		} catch (Exception e) {
			A[2] = 7;
			System.out.println(Arrays.toString(A));

		} finally {
			A[2] = A[2] + 5;
			System.out.println(Arrays.toString(A));
		}

		try {
			System.out.println(A());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static int A() throws Exception {
		int A[] = new int[3];
		A[3] = 7;
		return A[0] + A[1] + A[2] + A[3];
	}

}
