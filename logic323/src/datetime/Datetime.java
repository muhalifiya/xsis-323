package datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Datetime {

	public static void main(String[] args) {

		LocalDate date = LocalDate.now();
		System.out.println(date);

		LocalTime time = LocalTime.now();
		System.out.println(time);

		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(dateTime);

		// format Date Time
		DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		String formattedDate = dateTime.format(formatDate);
		System.out.println(formattedDate);

		// menampilkan beberapa hari setelah tanggal hari ini:
		LocalDate localDate = LocalDate.now();
		LocalDate tgl = localDate.plusDays(5); // 5 hari setelah tanggal 17 / hari ini
		System.out.println(tgl);

		LocalDate bulan = localDate.plusMonths(3); // 3 bulan setelah tanggal hari ini
		System.out.println(bulan);

		LocalDate pekan = localDate.plusWeeks(2); // 2 pekan setelah tanggal hari ini
		System.out.println(pekan);

		LocalDate tahun = localDate.plusYears(2); // 2 tahun setelah tanggal hari ini
		System.out.println(tahun);

		// mencari selisih hari
		LocalDate startDate = LocalDate.parse("2022-01-01");
		LocalDate endDate = LocalDate.parse("2023-01-01");

		long dayCount = ChronoUnit.DAYS.between(startDate, endDate);
		System.out.println(dayCount + " Hari");

		long monthCount = ChronoUnit.MONTHS.between(startDate, endDate);
		System.out.println(monthCount + " Bulan");

		long yearCount = ChronoUnit.YEARS.between(startDate, endDate);
		System.out.println(yearCount + " Tahun");

		System.out.println();

		// mencari selisih datetime
		LocalDateTime startTime = LocalDateTime.parse("2022-01-28T07:30:34");
		LocalDateTime endTime = LocalDateTime.parse("2022-01-29T20:03:34");

		long second = ChronoUnit.SECONDS.between(startTime, endTime);
		System.out.println(second + " detik");

		long hours = ChronoUnit.HOURS.between(startTime, endTime);
		System.out.println(hours + "jam");
		long minutes = ChronoUnit.MINUTES.between(startTime, endTime);
		System.out.println(minutes - (60 * hours) + " menit");
		long days = ChronoUnit.DAYS.between(startTime, endTime);
		System.out.println(days);
		long months = ChronoUnit.MONTHS.between(startTime, endTime);
		System.out.println(months);
		long years = ChronoUnit.YEARS.between(startTime, endTime);
		System.out.println(years);

		System.out.println();

		// konversi dari 12 jam (AM/PM) ke 24 JAM atau sebaliknya
		final String waktu = "23:15";

		String result = LocalTime.parse(waktu, DateTimeFormatter.ofPattern("HH:mm"))
				.format(DateTimeFormatter.ofPattern("hh:mm a"));
		System.out.println(result);

		final String waktu2 = "09:15 PM";
		String result2 = LocalTime.parse(waktu2, DateTimeFormatter.ofPattern("hh:mm a"))
				.format(DateTimeFormatter.ofPattern("HH:mm"));
		System.out.println(result2);
	}

}
