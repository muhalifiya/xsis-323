package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Array2d_day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[][] A = new int[2][2];

		A[0][0] = 9;
		A[0][1] = 3;
		A[1][0] = 10;
		A[1][1] = 5;

		System.out.println(Arrays.deepToString(A));

		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				System.out.println(A[i][j] + " ");
			}
			System.out.println();

		}
		int[][] B = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, 10 } };
		printInt2dArray(B);

		Scanner scan = new Scanner(System.in);
		System.out.print("Masukkan panjang baris = ");
		int baris = scan.nextInt();
		System.out.print("Masukkan panjang kolom = ");
		int kolom = scan.nextInt();

//		int x = 2, y = 2;
		int[][] simpan = inputArray(baris, kolom);
		printInt2dArray(simpan);

		int[][] simpan2 = inputArray(baris, kolom);
		printInt2dArray(simpan2);

		// operasi pertambahan matrix simpan + simpan2
//		int[][] hasil = new int[x][y];
//		for (int i = 0; i < hasil.length; i++) {
//			for (int j = 0; j < hasil[i].length; j++) {
//				hasil[i][j] = simpan[i][j] + simpan2[i][j];
//			}
//		}
		int[][] hasil = penjumlahan(baris, kolom, simpan, simpan2);
		printInt2dArray(hasil);

	}

	public static int[][] penjumlahan(int x, int y, int simpan[][], int simpan2[][]) {
		int[][] temp = new int[x][y];
		for (int i = 0; i < temp.length; i++) {
			for (int j = 0; j < temp[i].length; j++) {
				temp[i][j] = simpan[i][j] + simpan2[i][j];
			}
		}

		return temp;

	}

	public static void printInt2dArray(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();

		}

	}

	public static int[][] inputArray(int baris, int kolom) {

		int[][] temp = new int[baris][kolom];
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < temp.length; i++) {
			for (int j = 0; j < temp[i].length; j++) {
				System.out.print("Input data ke [" + i + "] [" + j + "] = ");
				temp[i][j] = scan.nextInt();
			}

		}

		return temp;
	}

}
