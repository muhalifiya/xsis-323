package arrays;

import java.util.Arrays; //-> domain.package.class
import java.util.Collections;
import java.util.Scanner;

public class BelajarArrays_day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int empId[];
		empId = new int[5];

		empId[0] = 10;
		empId[1] = 20;
		empId[2] = 30;
		empId[3] = 40;
		empId[4] = 50;

		// pembacaan arrays menggunakan class array
		System.out.println(Arrays.toString(empId));

		// pembacaan array menggunakan for-loop
		for (int i = 0; i < empId.length; i++) {
			System.out.print(empId[i] + ",");

		}
		System.out.println();

		int[] A = new int[5];
		int year = 2000;

		for (int i = 0; i < A.length; i++) {
			A[i] = year;
			year++;

		}
		System.out.println(Arrays.toString(A));

		printArrays(empId);
		printArrays(A);

		// penulisan array

		int[] D = { 11, 6, 3, 1, 7, 9, 10 };

		String[] strArr = { "Mercedes", "Jaguar", "Bently", "Maserati", "Bajaj" };

		printStrArrays(strArr);

		char[] charArr = { 'A', 'B', 'D', 'G', 'I', 'C', 'E' };
		printCharArrays(charArr);

		double[] doubleArrays = { 1.2, 3.1, 1.1, 2.2, 3.2 }; // d kecil premitive

		printDoubleArrays(doubleArrays);

		printArrays(D);// sebelum disort
		Arrays.sort(D);
		printArrays(D);// setelah disort

		Arrays.sort(charArr);
		printCharArrays(charArr);

		Arrays.sort(doubleArrays);
		printDoubleArrays(doubleArrays);
		System.out.println(doubleArrays[3]);

		Double[] dArrays = { 1.2, 3.1, 1.1, 2.2, 3.2 }; // Double -> tipe data non premitive
		Arrays.sort(dArrays, Collections.reverseOrder()); // sort descending(besar ke kecil)
		printDoubleArrays(dArrays);

		System.out.println();

		// int out[]= 6,9,7,6,8
//		int Out[] = new int[5];
//		int B[] = { 5, 7, 4, 2, 3 };
//		int[] C = { 1, 2, 3, 4, 5 };
//		for (int i = 0; i < Out.length; i++) {
//			Out[i] = B[i] + C[i];
//			System.out.print(Out[i] + " ");
//		}

//		penjumlahan(B, C);

		Scanner scan = new Scanner(System.in);
		System.out.print("Masukkan panjang array = " );
		int x = scan.nextInt();

		int[] B = inputArray(x);
		printArrays(B);

		int[] C = inputArray(x);
		printArrays(C);

		
		penjumlahan(x, B, C);
	}

	public static int[] inputArray(int x) {
		int[] temp = new int[x];
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < temp.length; i++) {
			System.out.print("Input data ke [" + i + "]  = ");
			temp[i] = scan.nextInt();
		}

		return temp;
	}

	public static int[] penjumlahan(int x, int[] B, int[] C) {
		int Out[] = new int[x];
		for (int i = 0; i < Out.length; i++) {
			Out[i] = B[i] + C[i];
			System.out.print(Out[i] + " ");
		}
		return Out;
	}

	public static void printArrays(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
		System.out.println();
	}

	public static void printStrArrays(String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
		System.out.println();
	}

	public static void printCharArrays(char[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
		System.out.println();
	}

	public static void printDoubleArrays(double[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
		System.out.println();
	}

	public static void printDoubleArrays(Double[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ",");
		}
		System.out.println();
	}

}
