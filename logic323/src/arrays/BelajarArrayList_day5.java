package arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class BelajarArrayList_day5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<String> cars = new ArrayList<String>();

		cars.add("Mercedes");
		cars.add("Jaguar");
		cars.add("Maybach");

		System.out.println(cars);

		for (int i = 0; i < cars.size(); i++) { // .size
			cars.set(1, "Audi");
			System.out.print(cars.get(i) + " "); // .get
		}

		System.out.println();
		cars.set(1, "Bajaj"); // .set

		for (String c : cars) { // disebut sebagai foreach
			System.out.print(c + " ");
		}
		System.out.println();

		cars.remove(1); // .remove
		System.out.println(cars);

		cars.clear();// .clear
		System.out.println(cars);
		System.out.println("==========================================");

		String o = "hackerworld";
		String s = "hackerrank";
		String result = "";
		int j = 0;
		char x = s.charAt(j);
		for (int i = 0; i < o.length(); i++) {
			if (o.charAt(i) == x) {
				result += o.charAt(i);
				if (j == s.length() - 1) {
					break;
				} else {
					j += 1;
					x = s.charAt(j);
				}

			}

		}

		System.out.println(result);

		System.out.println("==========================================");

		String s1 = "absdjkvuahdakejfnfauhdsaavasdlkj"; // n
		String s2 = "djfladfhiawasdkjvalskufhafablsdkashlahdfa"; // wi

//		String s1 = "abc";
//		String s2 = "amnop";
		int flag = 0;
		String result1 = "";
		char tmp = s1.charAt(flag);
		char tmp1 = s2.charAt(flag);

		if (s1.length() > s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				if (s1.charAt(i) == tmp1) {
					result1 += s1.charAt(i);
					if (flag == s2.length() - 1) {
						break;
					} else {
						flag += 1;
						tmp1 = s2.charAt(flag);
					}
				}
			}
		} else {
			for (int i = 0; i < s2.length(); i++) {
				if (s2.charAt(i) == tmp) {
					result1 += s2.charAt(i);
					if (flag == s1.length() - 1) {
						break;
					} else {
						flag += 1;
						tmp = s1.charAt(flag);
					}
				}

			}
		}

		System.out.println(result1);

		int x1 = s1.length() - result1.length();
		int x2 = s2.length() - result1.length();

		System.out.println(x1 + x2);

		System.out.println("==============================");

		String a = "aardvark";
		String b = "apple";
		String hasil = "";
		int count = 0;
		if (a.length() > b.length()) {
			for (int i = 0; i < a.length(); i++) {
				for (int j1 = 0; j1 < b.length(); j1++) {
					if (a.charAt(i) == b.charAt(j1)) {
						count++;
						break;
					}
				}
			}
		} else {
			for (int i = 0; i < b.length(); i++) {
				for (int j1 = 0; j1 < a.length(); j1++) {
					if (b.charAt(i) == a.charAt(j1)) {
						count++;
						break;
					}
				}
			}
		}

		System.out.println(hasil);

		System.out.println(count);

	}

}
