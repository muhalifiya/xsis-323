package arrays;

import java.util.Arrays;

public class Median_day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int median[] = { 5, 7, 4, 2, 3 };
		System.out.println(Arrays.toString(median));

		median(median);

		int[] median1 = { 3, 2, 3, 1, 5, 7 };
		System.out.println(Arrays.toString(median1));

		median(median1);

	}

	public static void median(int[] median) {

		Arrays.sort(median);

		System.out.println(Arrays.toString(median));
		if (median.length % 2 == 0) {
			System.out.println((float) (median[median.length / 2] + median[(median.length / 2) - 1]) / 2);
		} else {
			System.out.println(median[median.length / 2]);
		}

	}

}
