package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BelajarList_day5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> fruit = new ArrayList<String>();

		fruit.add("Aple");
		fruit.add("Orange");
		fruit.add("Avocado");

		System.out.println(fruit);

		for (String f : fruit) {
			System.out.print(fruit + " ");
		}
		System.out.println();

		for (String f : fruit) {
			System.out.print(f + " ");
		}
		System.out.println();

//		System.out.println(fruit.size());
//		Collections.sort(fruit); // ascending
//		System.out.println(fruit);
//		Collections.reverse(fruit); // descending
//		System.out.println(fruit);

//		fruit.set(2, "Manggo");
//		System.out.println(fruit);

//		fruit.remove(0);
//		System.out.println(fruit);

//		fruit.clear();
//		System.out.println(fruit);

//		String[] bahasa = { "Jawa", "Sunda", "Batak" };
//		System.out.println(Arrays.toString(bahasa));
//
//		// convert from string array to list
//		List<String> lang = new ArrayList<String>();
//		for (String l : bahasa) {
//			lang.add(l);
//		}
//		System.out.println(lang);
//
//		for (String l : lang) {
//			System.out.print(l + " ");
//			;
//		}
//
//		System.out.println();
//
//		// convert from list to string array
//		String[] language = lang.toArray(new String[lang.size()]);
//		System.out.println(Arrays.toString(language));
//		System.out.println("===================================");
//
//		Set<Character> resultSet = new HashSet();
//		Set<Character> resultSet1 = new HashSet();
//		String a = "hi";
//		String b = "world";
//		for (int i = 0; i < a.length(); i++) {
//			resultSet.add(a.charAt(i));
//		}
//
//		for (int i = 0; i < b.length(); i++) {
//			resultSet1.add(b.charAt(i));
//		}
//
//		char[] x = new char[resultSet.size()];
//		int i = 0;
//
//		for (Character f : resultSet) {
//			x[i] = f;
//			i++;
//		}
//
//		i = 0;
//		char[] y = new char[resultSet1.size()];
//		for (Character f : resultSet1) {
//			y[i] = f;
//			i++;
//		}
//
//		System.out.println(Arrays.toString(x));
//		System.out.println(Arrays.toString(y));
//
//		boolean check = false;
//		for (int k = 0; k < x.length; k++) {// 0 //1
//			for (int l = 0; l < y.length; l++) {
//				if (x[k] == y[l]) { // e == h // h == i
//					check = true;
//					break;
//				}
//
//			}
//
//		}
//
//	    System.out.println(check);

	}

}
