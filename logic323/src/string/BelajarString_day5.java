package string;

import java.util.Arrays;
import java.util.Scanner;

public class BelajarString_day5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a = "";
		String b = "baris 1" + " baris 2";

		System.out.println(a);
		System.out.println(b);

		System.out.println("Baris 3 Baris 4");
		// escape character
		System.out.println("Baris 3\nBaris 4");
		System.out.println("Baris 5\rBaris 6");
		System.out.println("Baris 7\tBaris 8");
		System.out.println("\'Baris 9\'\t\"Baris 10\"");

		System.out.println();

		String str1 = new String();// empty string
		String str4 = new String(str1);
		String str2 = new String("");
		String str3 = new String("Hello");

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);

		System.out.println(str3.length());

		int len1 = "".length();
		System.out.println(len1);
		int len2 = "Hello".length();
		System.out.println(len2);
		System.out.println();

		// immutable string
		String str;
		str = new String("String01"); // str -> referance
		System.out.println(str);
		str = new String("String02");
		System.out.println(str);
		System.out.println();

		String str5 = "";
		String str6 = "Another String";
		String str7 = "";

		boolean b1 = str5.equals(str6);
		System.out.println(b1);
		boolean b2 = str5.equals(str7);
		System.out.println(b2);
		System.out.println();

		// string compare
		System.out.println("abc".compareTo("abc"));
		System.out.println("abc".compareTo("xyz"));
		System.out.println("xyz".compareTo("abc"));

		System.out.println("abc".compareTo("abd"));
		System.out.println("Abc".compareTo("abd"));
		System.out.println("ABC".compareTo("ABd"));

		// charAt
		String xsis = "Xsis Academy";
		int len = xsis.length();

		for (int i = 0; i < len; i++) {
			char x = xsis.charAt(i);
			System.out.println("Char " + x + " di index ke " + i);

		}

		String s1 = "hello";
		String s2 = "HELLo";

//		if (s1.equals(s2)) {
//			System.out.println("s1 == s2");
//		} else {
//			System.out.println("s1 != s2");
//		}
//
//		if (s1.equalsIgnoreCase(s2)) {
//			System.out.println("s1 == s2");
//		} else {
//			System.out.println("s1 != s2");
//		}

		if (s1.equals(s2))
			System.out.println("s1 == s2");
		else
			System.out.println("s1 != s2");

		if (s1.equalsIgnoreCase(s2))
			System.out.println("s1 == s2");
		else
			System.out.println("s1 != s2");

		// stringEmpty
		String s3 = "Xsis";
		String s4 = "";

		boolean notEmpty = s3.isEmpty();
		boolean isEmpty = s4.isEmpty();
		System.out.println(notEmpty);
		System.out.println(isEmpty);
		System.out.println();

		// to upper to lower
		String code = "XsiS Academy";
		System.out.println(code.toUpperCase());
		System.out.println(code.toLowerCase());
		System.out.println();

		String xa = "XSIS";
		int index;
		index = xa.indexOf("X");
		System.out.println(index);
		index = xa.indexOf("S");
		System.out.println(index);
		index = xa.lastIndexOf("S");
		System.out.println(index);

		// valueOf
		System.out.println(String.valueOf('A'));//  'A'    ->   "A"
		System.out.println(String.valueOf("15"));// "15"   ->   "15"
		System.out.println(String.valueOf(true));// true   ->   "true"
		System.out.println(String.valueOf(1996));// 1996   ->   "1996"
		System.out.println();
		
		//substring
		String sub ="Xsis Academy";
		System.out.println(sub.substring(1)); // mulai dari index 1
		System.out.println(sub.substring(1, 6)); //mulai dari index 1 sampai ke 6 tapi hanya mencetak sampai index ke 5
		
		
		//trimming -> menghilangkan white space di depan/belakang atau depan dan belakang tapi tidak di tengah
		String trim1 = "  XA  ".trim();
		String trim2 = "XA  CA ".trim();
		String trim3 = "\n\n \r\tXA		\t".trim();
		System.out.println(trim1);
		System.out.println(trim2);
		System.out.println(trim3);
		System.out.println();
		
		
		//string replace
		String gigi = "Tooth";
		System.out.println(gigi);
		System.out.println(gigi.replace('o','e'));
		System.out.println();
		
		//star & end with
		String java = "This is java program";
		System.out.println(java.startsWith("This"));
		if(java.startsWith("This")) {
			System.out.println("Start with \"This\"");
		}else {
			System.out.println("Not start with \"This\"");
		}
		
		if(java.endsWith("This")) {
			System.out.println("End with \"This\"");
		}else {
			System.out.println("Not end with \"This\"");
		}
		System.out.println();
		
		//splitting and joining string
		String asean = "INA,MAL,SIG,THA,MYA";
		String[]country=asean.split(",");
		System.out.println(Arrays.toString(country));
		
		String aseanJoin = String.join(",", country);
		System.out.println(aseanJoin);
		
		//toCharArray
		String word = "Xsis Academy";
		char[] charArr = word.toCharArray();
		System.out.println(Arrays.toString(charArr));
		
		//indexOf & contains
		String office = "Xsis academy kebayoran baru";
		System.out.println(office.indexOf("kebayoran"));
		
		System.out.println(office.contains("Xsis"));
		System.out.println(office.contains("baru "));
		System.out.println();
		
		//input -> "7 6 1 3 2 5 9 5"
		Scanner scan = new Scanner(System.in);
		System.out.println("Silahkan masukkan rangkaian angka (batasi dengan spasi): ");
		String stringInput = scan.nextLine();
		String [] strArray = stringInput.split(" ");
		Integer[] intArray = new Integer[strArray.length];
		
		for(int i = 0; i < strArray.length; i++) {
			intArray[i] = Integer.parseInt(strArray[i]);
		}
		
		System.out.println(Arrays.toString(intArray));
		System.out.println();
		
		//find string with regex(regular expression)
		String newStr= "World$##$@! Wide Web(W3)".replaceAll("[^A-Za-z0-9 ()]+", "");
		System.out.println(newStr);
				
		
	}

}
