package class1;

import class2.Race;
import class2.WaraWiri;

public class Main {

	public static void main(String[] args) { // entry point
		// TODO Auto-generated method stub

		// instansiasi
		Human human = new Human();
		human.name = "Peter"; //renewable harus assignment ulang, ini seperti property di suatu class
		human.gender = "Male";
		human.pob = "Bogor"; //static sifat nya sama seperti variable
		
		System.out.println(human.getName() + " " + human.pob);
		
		Human Mary = new Human();
//		Mary.name = "Mary"; //renewable harus assignment ulang
		Mary.gender = "Female";
		
		Mary.pob = "Garut"; //static sifat nya sama seperti variable
		System.out.println(Mary.getName() + " " + Mary.pob );
		
		
		System.out.println();
		
		
		Race peterRace = new Race(); // new Race() -> instance class
		peterRace.origin ="American";
		peterRace.region ="California";
		System.out.println(peterRace.getOrigin());
		
		System.out.println();
		
		Race maryRace = new Race(); //Race() default dari property/atribut/field region adalah earth
		maryRace.origin = "American";
		System.out.println(maryRace.getOrigin());
		
		Race other = new Race("Bumi", "Depok"); // constructor yang berbeda dengan yang diatas nya
		
		System.out.println(other.getOrigin());
		
		Human peter = new Human();
		peter.setPhoneNumber("021");
		System.out.println(peter.getPhoneNumber());
		
		Childs childs = new Childs();
		childs.address = "Cibinong";// -> langsung ke properti
		System.out.println(childs.address);
		childs.setAddress("Citayam");//-> melalui methods
		System.out.println(childs.address);
		System.out.println(childs.taxNumber); // final property bisa diakses
		//childs.taxNumber = "777"; taxNumber final tidak bisa dirubah / di assignment ulang
		
		System.out.println();
		
		Bentor bentor =new Bentor();
		bentor.uniqVehicle(); // anaknya //Becak Motor
		other.uniqVehicle(); // bapaknya //Kendaraan unik
		
		WaraWiri waraWiri = new WaraWiri();
		waraWiri.uniqVehicle();
		
		System.out.println();
		
		Race kendaraan = new Race();
		Race becakMotor = new Bentor();
		Race warwaWiri2 = new WaraWiri();
		
		kendaraan.uniqVehicle();
		becakMotor.uniqVehicle();
		waraWiri.uniqVehicle();
		
		
		
		
		
		
		

	}

}
