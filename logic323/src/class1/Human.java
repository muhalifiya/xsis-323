package class1;

public class Human {
	String name; // instance property
	String gender; // instance property
	static String pob; // static property

	private String phoneNumber;
	private String idNumber;
	
	protected String address; //protected property
	
	public final String taxNumber = "7562345-888";//
	
	public String getName() {
		return name + " (" + gender + ")";
	}
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	
	

}
