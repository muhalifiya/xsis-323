package com.coba.coba.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coba.coba.models.Car;
import com.coba.coba.repositories.CarRepository;

@RestController
@RequestMapping("/api/")
@CrossOrigin("*")
public class CarRestController {

    @Autowired
    private CarRepository carRepository;

    @GetMapping("/car")
    public ResponseEntity<List<Car>> getCar() {
        try {
            List<Car> car = this.carRepository.findAll();
            return new ResponseEntity<List<Car>>(car, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Car>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<?> getCarById(@PathVariable Long id) {
        try {
            Car car = this.carRepository.findById(id).orElse(null);
            if (car != null) {
                return new ResponseEntity<Car>(car, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Id not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Car>>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/car")
    public ResponseEntity<Car> addCar(@RequestBody Car car) {
        try {
            this.carRepository.save(car);
            return new ResponseEntity<Car>(car, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Car>(HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping("/car/{id}")
    public ResponseEntity<Car> updateCar(@RequestBody Car car, @PathVariable Long id) {
        try {
            car.setId(id);
            this.carRepository.save(car);
            return new ResponseEntity<Car>(car, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Car>(car, HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/car/{id}")
    public ResponseEntity<?> deleteCar(@PathVariable Long id) {
        try {
            Car car = this.carRepository.findById(id).orElse(null);
            if (car != null) {
                this.carRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("Car berhasil terhapus");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Delete failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
