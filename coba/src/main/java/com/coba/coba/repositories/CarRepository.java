package com.coba.coba.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coba.coba.models.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

}
