package com.coba.coba.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nama", length = 50)
    private String Nama;

    @Column(name = "type", length = 10)
    private String type;

    @Column(name = "tahunKeluar")
    private Date tahunKeluar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTahunKeluar() {
        return tahunKeluar;
    }

    public void setTahunKeluar(Date tahunKeluar) {
        this.tahunKeluar = tahunKeluar;
    }

}
