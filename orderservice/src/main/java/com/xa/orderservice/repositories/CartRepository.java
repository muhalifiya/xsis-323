package com.xa.orderservice.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.orderservice.models.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    @Query(value = "SELECT * FROM cart WHERE customer_id = ?1", nativeQuery = true)
    List<Cart> getCustomerIdFromCart(Long CustomerId);

}
