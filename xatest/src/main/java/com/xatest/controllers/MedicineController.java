package com.xatest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xatest.models.Medicine;

@Controller
@RequestMapping("/medicine")
public class MedicineController {

	@GetMapping("")
	public ModelAndView index() {// untuk menerapkan design pattern MVC dari spring. 
		                         // ModelAndView ini seperti tipe data 
		
		ModelAndView view = new ModelAndView("medicine/index");
		Medicine medicine = new Medicine();
		medicine.setName("Amlodipine");
		medicine.setDosage(1);
		view.addObject("dataMedicine", medicine);
		return view;
	}
}
