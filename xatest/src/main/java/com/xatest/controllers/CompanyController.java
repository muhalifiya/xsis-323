package com.xatest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/company") // url
public class CompanyController {
	
	@GetMapping("index") // sub url
	public String index() {
		return "/company/index";
	}
	
	@GetMapping("about")
	public String about() {
		return "/company/about";
	}

}
