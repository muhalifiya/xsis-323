package com.xatest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cars")
public class CarsController {
	
	@GetMapping("{brand}")//sebagai value/nilai yang akan diambil menggunakan @PathVariable
	@ResponseBody //membuat body pada html akan merespon dengan route yang berikut nya dijadikan value/nilai untuk parameter
	public String brand(@PathVariable("brand") String merek) { // @PathVariable("brand") menjadikan "{brand}" sebagai variable, kemudian akan dimasukkan pada String merek, yang memiliki tipe data string
		String myCars = "My car is : " + merek;
		return myCars;
	}
	
	@GetMapping("{coba}/{type}")
	@ResponseBody
	public String a(@PathVariable("coba") String b, @PathVariable String type) {
		String myCars = "My car is : " + b + ", type : " + type;
		return myCars;
	}
	
//	@GetMapping("{brand}/{type}")
//	@ResponseBody
//	public String c(@PathVariable String brand, @PathVariable String type) {
//		String myCars = "My car is : " + brand + ", type : " + type;
//		return myCars;
//	}

}
