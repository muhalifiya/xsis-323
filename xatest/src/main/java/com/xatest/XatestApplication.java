package com.xatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XatestApplication {

	public static void main(String[] args) {
		SpringApplication.run(XatestApplication.class, args);
	}

}
