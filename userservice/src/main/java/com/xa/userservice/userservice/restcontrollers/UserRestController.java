package com.xa.userservice.userservice.restcontrollers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.userservice.userservice.models.User;
import com.xa.userservice.userservice.repositories.UserRepository;

@RestController
@RequestMapping("/userservice/")
@CrossOrigin("*")
public class UserRestController {

    // ambil data dari table users
    // sambungkan ke repository(kita mau membuat query)

    @Autowired // menyambungkan class //tidak membuat menjadi turunan
    private UserRepository userRepository;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUser() {
        try {
            List<User> user = this.userRepository.findAll();
            return new ResponseEntity<List<User>>(user, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {
        try {
            User user = this.userRepository.findById(id).orElse(null);
            if (user != null) {
                return new ResponseEntity<User>(user, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("id not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @PostMapping("/users")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        try {
            System.out.println(user.getPassword());
            String pass = user.getPassword();

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodehash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));

            user.setPassword(bytesToHex(encodehash));

            this.userRepository.save(user);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/users/{id}") // {id} -> query string
    public ResponseEntity<User> editUser(@RequestBody User user, @PathVariable("id") Long id) {
        try {
            user.setId(id);
            this.userRepository.save(user);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) { // <?> -> optional bisa objek atau apapun dalam
                                                                       // return nya
        try {
            User user = this.userRepository.findById(id).orElse(null);
            if (user != null) {
                this.userRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("User Delete");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deletion failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/checklogin")
    public ResponseEntity<List<User>> checkLogin(@RequestBody User user) {
        try {
            String pass = user.getPassword();

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodehash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));

            List<User> getUser = this.userRepository.checkAccount(user.getUserName(), bytesToHex(encodehash));
            return new ResponseEntity<List<User>>(getUser, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
