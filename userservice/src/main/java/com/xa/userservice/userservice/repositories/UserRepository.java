package com.xa.userservice.userservice.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.userservice.userservice.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "SELECT * FROM users u WHERE u.UserName = ?1 AND u.Password = ?2", nativeQuery = true)
    List<User> checkAccount(String UserName, String Password);
}
