var host = 'http://localhost:8080';

function register(type) {
    var str = `
            <table width="100%" cellpadding="5">
                <tr>
                    <td>Username<br><input type="text" id="username" class="form-control"></td>
                </tr>
                <tr>
                    <td><span id="validasiUn" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>   
                    <td>Password<br><input type="password" id="password" class="form-control"></td>
                </tr>
                <tr>
                    <td><span id="validasiPs" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>
                    <td>Password Confirmation<br><input type="password" id="passcon" class="form-control"></td>
                </tr>
                <tr>
                    <td><span id="validasiPsc" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>
                    <td>First Name<br><input type="text" id="firstname" class="form-control"></td>
                </tr>
                <tr>
                    <td><span id="validasiFn" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>   
                    <td>Last Name<br><input type="text" id="lastname" class="form-control"></td>
                </tr>
                <tr>
                    <td><span id="validasiLn" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>
                    <td>Email<br><input type="email" id="email" class="form-control">
                        <input type="hidden" id="type" value="${type}" class="form-control">
                    </td>
                </tr>
                <tr>
                    <td> <span id="validasiEm" class="text-danger" style="font-size:12px"></span></td>
                </tr>
                <tr>
                    <td align="right"><button class="btn btn-primary" onclick="saveUser()">Save</button></td>
                </tr>
            </table>            
    `;


    $('#mymodal').modal('show');
    $('.modal-title').html('Registering User');
    $('.modal-body').html(str);
}

function saveUser() {
    var un = $('#username').val();
    var ps = $('#password').val();
    var psc = $('#passcon').val();
    var fn = $('#firstname').val();
    var ln = $('#lastname').val();
    var em = $('#email').val();
    var ty = $('#type').val();

    if (un == "" || ps == "" || psc == "" || fn == "" || ln == "" || em == "") {
        if (un == "") {
            $('#validasiUn').html("Username tidak boleh kosong!")
        }
        if (ps == "") {
            $('#validasiPs').html("Password tidak boleh kosong!")
        }
        if (psc == "") {
            $('#validasiPsc').html("Password Confirmation tidak boleh kosong!")
        }
        if (fn == "") {
            $('#validasiFn').html("First Name tidak boleh kosong!")
        }
        if (ln == "") {
            $('#validasiLn').html("Last Name tidak boleh kosong!")
        }
        if (em == "") {
            $('#validasiEm').html("Email tidak boleh kosong!")
        }
    } else {
        if (ps == psc) {
            const data = {
                userName: un,
                password: psc,
                firstName: fn,
                lastName: ln,
                email: em,
                status: "Active",
                type: ty
            }
            //console.log(data)
            $.ajax({
                url: 'http://localhost:9000/userservice/users',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (dataUser) {
                    //console.log(dataUser)

                    Swal.fire(
                        'Access Granted',
                        'Welcome to XA eCommerce',
                        'success'
                    )
                    //$('#mymodal').modal('hide')
                    //reloadData();
                    location.reload();
                }

            })

        } else {
            alert("Password Tidak Sama !")
        }

    }


}

const formatter = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR'
})