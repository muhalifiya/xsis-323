package com.xa.ecommerce.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.lowagie.text.DocumentException;
import com.xa.ecommerce.exporters.ProductExporter;
import com.xa.ecommerce.models.Products;
import com.xa.ecommerce.services.ProductService;

@Controller
@CrossOrigin("*")
public class HomeController {

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView view = new ModelAndView("/home");
        return view;
    }

    @GetMapping("/lookup")
    public ModelAndView lokkup() {
        ModelAndView view = new ModelAndView("lookup/index.html");
        return view;
    }

    @GetMapping("/store/category")
    public ModelAndView storeCategory() {
        ModelAndView view = new ModelAndView("store/category.html");
        return view;
    }

    @GetMapping("/store/product")
    public ModelAndView storeProduct() {
        ModelAndView view = new ModelAndView("store/product.html");
        return view;
    }

    @Autowired
    ProductService productService;

    @GetMapping("/store/product/pdf")
    public void exportToPdf(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("appllication/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyymmddHHmmss");
        String filename = dateFormat.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=product_" + filename + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<Products> listProduct = productService.allProduct();

        ProductExporter exporter = new ProductExporter(listProduct);
        exporter.export(response);
    }

    @GetMapping("/order")
    public ModelAndView order() {
        ModelAndView view = new ModelAndView("order/index.html");
        return view;
    }

    @GetMapping("/payment/{cart_id}")
    public ModelAndView payment() {
        ModelAndView view = new ModelAndView("payment/index.html");
        return view;
    }

}
