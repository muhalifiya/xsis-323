package com.xa.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.ecommerce.models.Categories;

@Repository
public interface CategoryRepository extends JpaRepository<Categories, Long> {

}
