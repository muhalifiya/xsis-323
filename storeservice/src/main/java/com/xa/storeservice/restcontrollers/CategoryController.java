package com.xa.storeservice.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.storeservice.models.Categories;
import com.xa.storeservice.repositories.CategoryRepository;

@RestController
@RequestMapping("/storeservice/")
@CrossOrigin("*")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/categories")
    public ResponseEntity<List<Categories>> getCategory() {
        try {
            List<Categories> category = this.categoryRepository.findAll();
            return new ResponseEntity<List<Categories>>(category, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Categories>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Long id) {
        try {
            Categories category = this.categoryRepository.findById(id).orElse(null);
            if (category != null) {
                return new ResponseEntity<Categories>(category, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("id not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/categories")
    public ResponseEntity<Categories> saveCategory(@RequestBody Categories category) {
        try {
            this.categoryRepository.save(category);
            return new ResponseEntity<Categories>(category, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Categories>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<Categories> editCategory(@RequestBody Categories category, @PathVariable("id") Long id) {
        try {
            category.setId(id);
            this.categoryRepository.save(category);
            return new ResponseEntity<Categories>(category, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Categories>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Long id) {
        try {
            Categories category = this.categoryRepository.findById(id).orElse(null);
            if (category != null) {
                this.categoryRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("Delete Succes");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("D=eletion failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
