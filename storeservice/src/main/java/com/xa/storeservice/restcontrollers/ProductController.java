package com.xa.storeservice.restcontrollers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.storeservice.models.Products;

import com.xa.storeservice.repositories.ProductRepository;

@RestController
@RequestMapping("/storeservice/")
@CrossOrigin("*")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/products")
    public ResponseEntity<List<Products>> getProduct() {
        try {
            List<Products> product = this.productRepository.getProductOrderById();
            return new ResponseEntity<List<Products>>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/productCategory")
    public ResponseEntity<List<Products>> getProductCategory() {
        try {
            List<Products> product = this.productRepository.getProductCategory();
            return new ResponseEntity<List<Products>>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/productCategoryMap")
    public ResponseEntity<List<Map<String, Object>>> getProductCategoryMap() {
        try {
            List<Map<String, Object>> product = this.productRepository.getProductCategoryMap();
            return new ResponseEntity<List<Map<String, Object>>>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Long id) {
        try {
            Products product = this.productRepository.findById(id).orElse(null);
            if (product != null) {
                return new ResponseEntity<Products>(product, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("id not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String address) throws Exception {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(address);
        msg.setSubject("New Product Registered");
        msg.setText("Product baru sudah di tambahkan ke sistem");
        javaMailSender.send(msg);

    }

    @PostMapping("/products")
    public ResponseEntity<Products> saveProduct(@RequestBody Products product) {
        try {
            this.productRepository.save(product);
            sendMail("muhammad.alifiya98@gmail.com");
            return new ResponseEntity<Products>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Products> editProduct(@RequestBody Products product, @PathVariable("id") Long id) {
        try {
            product.setId(id);
            this.productRepository.save(product);
            return new ResponseEntity<Products>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Products>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
        try {
            Products product = this.productRepository.findById(id).orElse(null);
            if (product != null) {
                this.productRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("Delete succes");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("deletion failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/products/search/{param}")
    public ResponseEntity<List<Products>> searchProduct(@PathVariable String param) {
        // try {
        List<Products> product = this.productRepository.searchProductByName(param);
        return new ResponseEntity<List<Products>>(product, HttpStatus.OK);
        // } catch (Exception e) {
        // e.printStackTrace();
        // return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        // }
    }

    @PatchMapping("/products/{id}/{stock}")
    public ResponseEntity<Products> reduceStock(@PathVariable Long id, @PathVariable Integer stock) {
        try {
            Products product = this.productRepository.findById(id).get();
            Integer st = product.getStock();
            product.setStock(st - stock);
            return new ResponseEntity<Products>(productRepository.save(product), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/products/increase/{id}/{stock}")
    public ResponseEntity<Products> increaseStock(@PathVariable Long id, @PathVariable Integer stock) {
        try {
            Products product = this.productRepository.findById(id).get();
            Integer st = product.getStock();
            product.setStock(st + stock);
            return new ResponseEntity<Products>(productRepository.save(product), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getValidate/{sku}/{name}")
    public ResponseEntity<Long> getValidate(@PathVariable String sku, @PathVariable String name) {
        try {
            Long product = this.productRepository.getValidate(sku, name);
            return new ResponseEntity<Long>(product, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
