package com.xa.storeservice.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xa.storeservice.models.Products;

@Repository

public interface ProductRepository extends JpaRepository<Products, Long> {

    @Query(value = "SELECT * FROM products WHERE name ILIKE %:searchText%", nativeQuery = true)
    List<Products> searchProductByName(@Param("searchText") String searchText);

    @Query(value = "SELECT * FROM products ORDER BY id", nativeQuery = true)
    List<Products> getProductOrderById();

    @Query(value = "SELECT * FROM products p LEFT JOIN categories c ON c.id=p.category_id", nativeQuery = true)
    List<Products> getProductCategory();

    @Query(value = "SELECT c.name AS category, " +
            "p.name AS product," +
            "p.description," +
            "p.price," +
            "p.volume," +
            "p.stock " +
            " FROM products p LEFT JOIN categories c ON c.id=p.category_id", nativeQuery = true)
    List<Map<String, Object>> getProductCategoryMap();

    @Query(value = "SELECT count(*) as banyakData FROM products WHERE sku=?1 AND name=?2", nativeQuery = true)
    public Long getValidate(String sku, String name);

}
