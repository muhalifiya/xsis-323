package com.xa.paymentservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.paymentservice.models.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

}
