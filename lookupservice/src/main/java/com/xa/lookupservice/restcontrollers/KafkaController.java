package com.xa.lookupservice.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xa.lookupservice.models.LookUp;
import com.xa.lookupservice.repositories.LookUpRepository;

import com.xa.lookupservice.services.Producer;

@RestController
@RequestMapping("/kafka")
public class KafkaController {
    private final Producer producer;

    @Autowired
    LookUpRepository LookUpRepository;

    @Autowired
    public KafkaController(Producer producer) {
        this.producer = producer;
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic() {
        List<LookUp> lookup = this.LookUpRepository.findAll();

        ObjectMapper object = new ObjectMapper();

        try {
            String jsonString = object.writeValueAsString(lookup);
            this.producer.sendMessage(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
