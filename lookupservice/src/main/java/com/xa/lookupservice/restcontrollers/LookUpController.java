package com.xa.lookupservice.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.lookupservice.models.LookUp;
import com.xa.lookupservice.repositories.LookUpRepository;

@RestController
@RequestMapping("/lookupservice/")
@CrossOrigin("*")
public class LookUpController {

    @Autowired // menyambungkan class //tidak membuat menjadi turunan
    private LookUpRepository lookUpRepository;

    @GetMapping("/lookups")
    public ResponseEntity<List<LookUp>> getLookUp() {
        try {
            List<LookUp> lookUp = this.lookUpRepository.findAll();
            return new ResponseEntity<List<LookUp>>(lookUp, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<LookUp>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/lookups/{id}")
    public ResponseEntity<?> getLookUpById(@PathVariable Long id) {
        try {
            LookUp lookUp = this.lookUpRepository.findById(id).orElse(null);
            if (lookUp != null) {
                return new ResponseEntity<LookUp>(lookUp, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("id not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/lookups")
    public ResponseEntity<LookUp> saveLookUp(@RequestBody LookUp lookUp) {
        try {
            this.lookUpRepository.save(lookUp);
            return new ResponseEntity<LookUp>(lookUp, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<LookUp>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/lookups/{id}")
    public ResponseEntity<LookUp> editLookUp(@RequestBody LookUp lookUp, @PathVariable("id") Long id) {
        try {
            lookUp.setId(id);
            this.lookUpRepository.save(lookUp);
            return new ResponseEntity<LookUp>(lookUp, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<LookUp>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/lookups/{id}")
    public ResponseEntity<?> deleteLookUp(@PathVariable("id") Long id) {
        try {
            LookUp lookUp = this.lookUpRepository.findById(id).orElse(null);
            if (lookUp != null) {
                this.lookUpRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("Delete succes");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Deletion failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
